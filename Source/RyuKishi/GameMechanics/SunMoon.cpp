// Fill out your copyright notice in the Description page of Project Settings.


#include "SunMoon.h"
#include "Components/DirectionalLightComponent.h" 
#include "Kismet/GameplayStatics.h"
#include "../Character/MainPlayerController.h"

// Sets default values
ASunMoon::ASunMoon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetupDefaulSubobjects();
	SetupRoot();
	SetupValues();

}

void ASunMoon::SetupDefaulSubobjects()
{
	Sun = CreateDefaultSubobject<UDirectionalLightComponent>(TEXT("Sun"));
	Moon = CreateDefaultSubobject<UDirectionalLightComponent>(TEXT("Moon"));
}

void ASunMoon::SetupRoot()
{
	RootComponent = Sun;
	Moon->SetupAttachment(GetRootComponent());
}

void ASunMoon::SetupValues()
{
	Sun->Intensity = 3.0f;
	Sun->LightSourceAngle = 2.0f;
	Sun->bUseTemperature = true;
	Sun->Temperature = 6000.0f;
	Sun->ShadowAmount = 0.876190f;
	Sun->SetAffectDynamicIndirectLighting(true);
	Sun->bEnableLightShaftOcclusion = true;
	Sun->bEnableLightShaftBloom = true;
	Sun->OcclusionDepthRange = 0.0f;
	Sun->BloomScale = 0.07619f;
	Sun->BloomMaxBrightness = 1.868784f;
	Sun->DynamicShadowDistanceMovableLight = 50000.0f;
	Sun->FarShadowCascadeCount = 1;
	Sun->FarShadowDistance = 50000.0f;
	Sun->DistanceFieldShadowDistance = 48000.0f;
	Sun->bUseRayTracedDistanceFieldShadows = true;
	Sun->bUsedAsAtmosphereSunLight = true;
	Sun->bCastShadowsOnClouds = true;
	Sun->MaxDrawDistance = 450000.0f;
	Sun->MaxDistanceFadeRange = 2000000.0f;

	Moon->Intensity = 1.5f;
	Moon->LightColor = FColor(30, 34, 56);
	Moon->LightSourceAngle = 5.0f;
	Moon->ShadowAmount = 0.876190f;
	Moon->SetAffectDynamicIndirectLighting(true);
	Moon->bEnableLightShaftOcclusion = true;
	Moon->bEnableLightShaftBloom = true;
	Moon->OcclusionDepthRange = 0.0f;
	Moon->BloomScale = 0.07619f;
	Moon->BloomMaxBrightness = 3.0f;
	Moon->DynamicShadowDistanceMovableLight = 50000.0f;
	Moon->FarShadowCascadeCount = 1;
	Moon->FarShadowDistance = 50000.0f;
	Moon->DistanceFieldShadowDistance = 48000.0f;
	Moon->bUseRayTracedDistanceFieldShadows = true;
	Moon->bUsedAsAtmosphereSunLight = true;
	Moon->AtmosphereSunLightIndex = 1;
	Moon->bCastShadowsOnClouds = true;
	Moon->MaxDrawDistance = 450000.0f;
	Moon->MaxDistanceFadeRange = 2000000.0f;



	Moon->SetRelativeRotation(FRotator(0, 180.f, 0));
	RotationSpeed = -0.1875f;
	RotationVector = FRotator(1, 0, 0);

	bChangeSunRotation = false;
	DefaultRotationSpeed = RotationSpeed;

}


// Called when the game starts or when spawned
void ASunMoon::BeginPlay()
{
	// Set Sun on PlayerController
	AMainPlayerController* MainPlayerController = Cast<AMainPlayerController>(UGameplayStatics::GetPlayerController(this, 0));

	if (MainPlayerController)
	{
		MainPlayerController->Sun = this;
	}
	
	Super::BeginPlay();
	
	if (Sun->GetComponentRotation().Pitch < 0)
	{
		CurrentInterpValue = 0.011f;
		OnChangeLightNightDay.Broadcast(CurrentInterpValue);
	}
	else
	{
		CurrentInterpValue = 0.989f;
		OnChangeLightNightDay.Broadcast(CurrentInterpValue);
	}
	DefaultRotationSpeed = RotationSpeed;

	
}

// Called every frame
void ASunMoon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Sun->AddWorldRotation((RotationVector * RotationSpeed * DeltaTime).Quaternion());

	bCurrentTargetValue = Sun->GetComponentRotation().Pitch > 0;
	InterToTargetValue((float)bCurrentTargetValue);

	if (bChangeSunRotation)
	{
		SetSunRotationFinished();
	}
	else
	{
		OnSunPositionChange.Broadcast(Sun->GetComponentRotation().Pitch);
	}
}


void ASunMoon::InterToTargetValue(const float Target)
{

	if(!FMath::IsNearlyEqual(CurrentInterpValue, Target, 0.01f))
	{
		CurrentInterpValue = FMath::FInterpTo(CurrentInterpValue, Target, GetWorld()->GetDeltaSeconds(), 0.2f);
		OnChangeLightNightDay.Broadcast(CurrentInterpValue);
	}
	else if(CurrentInterpValue != Target)
	{
		CurrentInterpValue = Target;
		OnChangeLightNightDay.Broadcast(CurrentInterpValue);
	}
}

void ASunMoon::SetSunRotationFinished()
{
	// Check for CurrentAngle == TargetAngle
	if (FMath::IsNearlyEqual(Sun->GetComponentRotation().Pitch, CurrentTargetAngle, 2.f))
	{
		RotationSpeed = DefaultRotationSpeed;
		bChangeSunRotation = false;
		CurrentInterpValue = (float)bCurrentTargetValue;
		OnChangeLightNightDay.Broadcast(CurrentInterpValue);
	}
}

void ASunMoon::SetSunRotation(float TargetAngle, float SpeedMultiplier)
{
	CurrentTargetAngle = TargetAngle;
	RotationSpeed = DefaultRotationSpeed * SpeedMultiplier;
	bChangeSunRotation = true;
}

void ASunMoon::ForceSunRotation(const float& NewAngle)
{
	Sun->SetWorldRotation((RotationVector * NewAngle).Quaternion() * Sun->GetComponentRotation().Quaternion());
}
