// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DayNightLight.h"
#include "RectDayNightLight.generated.h"

/**
 * 
 */
UCLASS()
class RYUKISHI_API ARectDayNightLight : public ADayNightLight
{
	GENERATED_BODY()
	
public:

	ARectDayNightLight();


};
