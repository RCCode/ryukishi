// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "Collectable.generated.h"

/**
 * 
 */
UCLASS()
class RYUKISHI_API ACollectable : public AInteractable
{
	GENERATED_BODY()
	
public:
	
	ACollectable();


	UPROPERTY(EditAnywhere, Category = "Interactable | Mesh")
	// Mesh of Interactable
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, Category = "Interactable | Particles")
	class UParticleSystemComponent* InteractParticles;

private:

	class AMainPlayerState* MainPlayerState;

protected:

	virtual void SetupDefaultSubobject() override;

	virtual void SetupRoot() override;

	virtual void SetupValues() override;

	virtual void BeginPlay() override;

public:

	virtual void BeforeInteract(const FRotator& CameraRotation) override;

	virtual void Interact(class AMainCharacter* Character);

	virtual void ExecuteInteraction(class AMainCharacter* Character) override;

	UFUNCTION()
	virtual void EndCollectable(class UParticleSystemComponent* PSystem);


};
