// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUp.h"
#include "Engine\SkeletalMeshSocket.h"
#include "RyuKishi\Character\MainCharacter.h"
#include "Kismet\KismetSystemLibrary.h"
#include "Components\SphereComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"

APickUp::APickUp() 
{
	SetupDefaultSubobject();
	SetupRoot();
	SetupValues();
	PrimaryActorTick.bCanEverTick = true;
}

#pragma region private

void APickUp::PlayHitSound()
{
	if (HitSound && !HitAudio->IsPlaying())
	{
		HitAudio->Stop();
		HitAudio->Sound = HitSound;
		HitAudio->FadeIn(0.3f, HitSound->VolumeMultiplier);
	}
}

void APickUp::Detach()
{
	DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, true));
	Mesh->SetSimulatePhysics(true);
	SetCollision(ECollisionResponse::ECR_Block);
	bHasInteracted = false;
}

void APickUp::ResetPhysics()
{
	Mesh->SetPhysicsLinearVelocity(FVector::ZeroVector);
	Mesh->SetPhysicsAngularVelocityInDegrees(FVector::ZeroVector);
}

void APickUp::CheckDistanceThrow()
{
	if (LastThrowPosition.Size() > 0 && (LastThrowPosition - GetActorLocation()).Size() >= MaxThrowDistance)
	{
		ResetPhysics();
		ThrowRespawn();
		LastThrowPosition = FVector::ZeroVector;
	}
}

void APickUp::ThrowRespawn()
{
	LastThrowPosition += ThrowRespawnOffset;
	SetActorLocation(LastThrowPosition);
}

void APickUp::EquipToCharacter(AMainCharacter* Character)
{
	LastThrowPosition = FVector::ZeroVector;
	Mesh->SetSimulatePhysics(false);
	SetCollision(ECollisionResponse::ECR_Ignore);
	const USkeletalMeshSocket* RightHandSocket = Character->GetMesh()->GetSocketByName("RightHandSocket");

	if (RightHandSocket)
	{
		RightHandSocket->AttachActor(this, Character->GetMesh());
	}
}

void APickUp::SetCollision(ECollisionResponse Response)
{
	FCollisionResponseContainer Coll;
	Coll.Pawn = Response;
	Coll.Camera = Response;
	Coll.Visibility = Response;

	Mesh->SetCollisionResponseToChannels(Coll);
	CollisionVolume->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, Response);
}

#pragma endregion

#pragma region protected

void APickUp::SetupDefaultSubobject()
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	HitAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("HitAudio"));
	Super::SetupDefaultSubobject();
}

void APickUp::SetupRoot()
{
	RootComponent = Mesh;
	HitAudio->SetupAttachment(GetRootComponent());
	Super::SetupRoot();
}

void APickUp::SetupValues()
{
	Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Ignore);
	Mesh->SetSimulatePhysics(true);
	Mesh->SetNotifyRigidBodyCollision(true);
	TickInterval = 10;
	DeathZ = 0;
	RelativeDropDistance = 50.f;
	SetActorTickInterval(TickInterval);
	ImpulseSize = 3100.f;
	FSoundAttenuationSettings Set;
	Set.AttenuationShapeExtents.X = 600.0f;
	Set.FalloffDistance = 1200.0f;
	HitAudio->AdjustAttenuation(Set);

	MaxThrowDistance = 5000.f;
	ThrowRespawnOffset = FVector(-100.f, 0.f, -100.f);

	Super::SetupValues();
}

void APickUp::BeginPlay()
{
	Super::BeginPlay();

	InitLocation = GetActorLocation();
	SetActorTickInterval(TickInterval);
	Mesh->OnComponentHit.AddDynamic(this, &APickUp::OnMeshHit);
	if (HitAudio)
	{
		HitAudio->FadeIn(0.1f, 0.f);
	}
}

#pragma endregion

#pragma region public

void APickUp::ExecuteInteraction(AMainCharacter* Character)
{
	Super::ExecuteInteraction(Character);

	if (Character)
	{
		EquipToCharacter(Character);
	}
}

void APickUp::Tick(float DeltaTime)
{
	// Custom Tick set on TickInterval in sec
	CheckDeathZ();
	CheckDistanceThrow();
}

void APickUp::BeforeInteract(const FRotator& CameraRotation)
{
	if (!bHasInteracted)
	{
		Super::BeforeInteract(CameraRotation);
	}
}

void APickUp::Interact(AMainCharacter* Character)
{
	if (!bHasInteracted)
	{
		Super::Interact(Character);
	}
}

void APickUp::OnMeshHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor)
	{
		if (NormalImpulse.Size() > ImpulseSize)
		{
			PlayHitSound();
		}
	}
}

void APickUp::Throw(const FVector Direction, const float& Strength)
{
	LastThrowPosition = GetActorLocation();
	Detach();
	Mesh->AddImpulse(Direction * Strength);
}

void APickUp::Drop(const FVector Direction)
{
	LastThrowPosition = GetActorLocation();
	Detach();
	SetActorLocation(Direction * RelativeDropDistance + GetActorLocation(), true);
}

void APickUp::CheckDeathZ()
{
	if (GetActorLocation().Z < DeathZ)
	{
		ResetPhysics();
		SetActorLocation(InitLocation);
	}
}

#pragma endregion
