// Fill out your copyright notice in the Description page of Project Settings.


#include "TriggerSequence.h"
#include "Components/SphereComponent.h"
#include "Runtime/LevelSequence/Public/LevelSequence.h"
#include "Runtime/LevelSequence/Public/LevelSequencePlayer.h"
#include "Sound\SoundCue.h"
#include "Kismet\GameplayStatics.h"
#include "..\Character\MainCharacter.h"

// Sets default values
ATriggerSequence::ATriggerSequence()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	
	SetupAttachment();
	SetupRoot();
	SetupValues();
	
}

void ATriggerSequence::SetupValues()
{	
	Super::SetupValues();
	bIsPlaying = false;
}

// Called when the game starts or when spawned
void ATriggerSequence::BeginPlay()
{
	Super::BeginPlay();
	
	if (LevelSequence && LevelSequencePlayer == nullptr)
	{
		ALevelSequenceActor* Actor;
		LevelSequencePlayer = ULevelSequencePlayer::CreateLevelSequencePlayer(GetWorld(), LevelSequence, FMovieSceneSequencePlaybackSettings(), Actor);
	}
}

// Called every frame
void ATriggerSequence::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATriggerSequence::TriggerSphereOnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		AMainCharacter* Player = Cast<AMainCharacter>(OtherActor);
		if (Player && !bIsPlaying)
		{
			if (LevelSequencePlayer)
			{
				LevelSequencePlayer->Play();
			}
			if (VoiceOver)
			{
				UGameplayStatics::PlaySound2D(this, VoiceOver);
			}
			bIsPlaying = true;
		}
	}
}

void ATriggerSequence::TriggerSphereOnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	
}

