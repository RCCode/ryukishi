// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PointLightComponent.h"
#include "DayNightLightComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class RYUKISHI_API UDayNightLightComponent : public UPointLightComponent
{
	GENERATED_BODY()
	
public:

	UDayNightLightComponent();

	UPROPERTY(EditInstanceOnly, Category = Sun)
	// Pointer to Sun, set in Scene
	TSoftObjectPtr<class ASunMoon> Sun;

	UPROPERTY(EditAnywhere, Category = Light)
	// Reverse functionality
	bool bReverse;

private:

	UPROPERTY()
	// Intensity set on BeginPlay()
	float MaxIntensity;

protected:

	virtual void BeginPlay() override;


public:

	UFUNCTION()
	// Set Light Intensity based on Multiplier
	virtual void SetLightIntensity(float Multiplier);
};
