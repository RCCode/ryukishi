// Fill out your copyright notice in the Description page of Project Settings.


#include "DayNightLightComponent.h"
#include "SunMoon.h"

UDayNightLightComponent::UDayNightLightComponent()
{
	bReverse = false;
	MaxIntensity = 0;
}

void UDayNightLightComponent::BeginPlay()
{
	Super::BeginPlay();
	if (Sun != nullptr)
	{
		MaxIntensity = Intensity;
		Sun->OnChangeLightNightDay.AddDynamic(this, &UDayNightLightComponent::SetLightIntensity);
	}
}

void UDayNightLightComponent::SetLightIntensity(float Multiplier)
{
	// Reverse Functionality on bReverse
	Multiplier = bReverse ? 1 - Multiplier : Multiplier;
	SetIntensity((Multiplier * MaxIntensity));
}
