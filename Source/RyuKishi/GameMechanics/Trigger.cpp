// Fill out your copyright notice in the Description page of Project Settings.


#include "Trigger.h"
#include "Components/SphereComponent.h"

// Sets default values
ATrigger::ATrigger()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void ATrigger::SetupAttachment()
{
	TriggerSphere = CreateDefaultSubobject<USphereComponent>(TEXT("TriggerSphere"));
}

void ATrigger::SetupRoot()
{
	RootComponent = TriggerSphere;
}

void ATrigger::SetupValues()
{
	TriggerSphere->SetSphereRadius(600.f);
}


// Called when the game starts or when spawned
void ATrigger::BeginPlay()
{
	Super::BeginPlay();
	
	TriggerSphere->OnComponentBeginOverlap.AddDynamic(this, &ATrigger::TriggerSphereOnOverlapBegin);
	TriggerSphere->OnComponentEndOverlap.AddDynamic(this, &ATrigger::TriggerSphereOnOverlapEnd);
}

// Called every frame
void ATrigger::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATrigger::TriggerSphereOnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

}

void ATrigger::TriggerSphereOnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{

}

