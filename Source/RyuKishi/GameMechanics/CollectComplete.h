// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CollectComplete.generated.h"

UCLASS()
class RYUKISHI_API ACollectComplete : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACollectComplete();

	UPROPERTY(EditAnywhere, Category = Particles)
		UParticleSystemComponent* ParticleSystemComponentLeft;
	
	UPROPERTY(EditAnywhere, Category = Particles)
	UParticleSystemComponent* ParticleSystemComponentRight;

	UPROPERTY(EditAnywhere, Category = Audio)
	UAudioComponent* AudioComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	
	UFUNCTION()
	virtual void CheckCollectScoreReached(const int& TotalAmount, const int& CurrentAmount);
};
