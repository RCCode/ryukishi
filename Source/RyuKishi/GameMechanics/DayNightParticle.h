// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DayNightParticle.generated.h"

UCLASS()
class RYUKISHI_API ADayNightParticle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADayNightParticle();


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Particle)
	class UParticleSystemComponent* DayNightParticleSystem;

	UPROPERTY(EditInstanceOnly, Category = Sun)
	// Pointer to Sun, set in Scene
	TSoftObjectPtr<class ASunMoon> Sun;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DayNight)
	bool bReverse;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	// Set Particle active based on Multiplier
	virtual void SetDayNightParticle(float Multiplier);

};
