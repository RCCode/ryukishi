// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.generated.h"

UCLASS()
class RYUKISHI_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractable();

	UPROPERTY(EditAnywhere, Category = "Interactable | CollisionVolume")
	// Volume to trigger Events for Interaction
	class USphereComponent* CollisionVolume;


	UPROPERTY(EditAnywhere, Category = "Interactable | Widget")
	// UI element to see the hotkey for the player
	class UWidgetComponent* Widget;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interactable | Status")
	// Has the player interacted with this object?
	bool bHasInteracted;

protected:

	virtual void SetupDefaultSubobject();

	virtual void SetupRoot();

	virtual void SetupValues();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Rotate the widget based on CameraRotation, while the player is looking at this actor
	virtual void SetWidget(const FRotator& CameraRotation);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	// Called if the player is looking at this actor
	virtual void BeforeInteract(const FRotator& CameraRotation);

	UFUNCTION()
	// Called on Playerinteraction
	virtual void Interact(class AMainCharacter* Character);

	UFUNCTION()
	// Executes before EndInteract()
	virtual void ExecuteInteraction(class AMainCharacter* Character);

	UFUNCTION()
	// After ExecuteInteraction() end interaction
	virtual void EndInteract();

};
