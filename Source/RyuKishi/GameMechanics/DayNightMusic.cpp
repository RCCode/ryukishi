// Fill out your copyright notice in the Description page of Project Settings.


#include "DayNightMusic.h"
#include "Kismet/GameplayStatics.h"
#include "../Character/MainCharacter.h"
#include "SunMoon.h"

// Sets default values
ADayNightMusic::ADayNightMusic()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void ADayNightMusic::BeginPlay()
{
	if (Sun)
	{
		Sun->OnChangeLightNightDay.AddDynamic(this, &ADayNightMusic::SetLightMusic);
		Main = Cast<AMainCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	}
	Super::BeginPlay();
	

}

void ADayNightMusic::SetLightMusic(float Multiplier)
{
	if (Sun && Main)
	{
		if (Multiplier <= 0.f && Main->GetMusicStatus() != EMusicStatus::Music_Day)
		{
			SwitchDayNightMusic(EMusicStatus::Music_Day);
		}
		else if(Main->GetMusicStatus() != EMusicStatus::Music_Night)
		{
			SwitchDayNightMusic(EMusicStatus::Music_Night);
		}
	}
}

void ADayNightMusic::SwitchDayNightMusic(const EMusicStatus& Status)
{
	Main->SetMusicStatus(Status);
}

