// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DayNightLight.h"
#include "PointDayNightLight.generated.h"

/**
 * 
 */
UCLASS()
class RYUKISHI_API APointDayNightLight : public ADayNightLight
{
	GENERATED_BODY()
	
public:

	APointDayNightLight();

protected:

	virtual void SetupDefaultSubobject() override;

};
