// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Trigger.h"
#include "GameFramework/Actor.h"
#include "TriggerSequence.generated.h"

UCLASS()
class RYUKISHI_API ATriggerSequence : public ATrigger
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATriggerSequence();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sequence)
	class ULevelSequence* LevelSequence;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sequence)
	class USoundCue* VoiceOver;

private:

	UPROPERTY()
	class ULevelSequencePlayer* LevelSequencePlayer;

	UPROPERTY()
	bool bIsPlaying;

protected:

	virtual void SetupValues() override;
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	virtual void TriggerSphereOnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void TriggerSphereOnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
};
