// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableAnimInstance.h"
#include "AnimatedInteractable.h"


void UInteractableAnimInstance::NativeInitializeAnimation()
{
	AActor* Owner = GetOwningActor();
	if (Owner)
	{
		AnimatedInteractable = Cast<AAnimatedInteractable>(Owner);
	}
}
