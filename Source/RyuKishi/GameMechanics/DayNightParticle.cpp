// Fill out your copyright notice in the Description page of Project Settings.


#include "DayNightParticle.h"
#include "Particles\ParticleSystemComponent.h"
#include "SunMoon.h"

// Sets default values
ADayNightParticle::ADayNightParticle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	DayNightParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("DayNightParticleSystem"));
	DayNightParticleSystem->SetupAttachment(GetRootComponent());

	DayNightParticleSystem->SetAutoActivate(false);
	bReverse = false;

}

// Called when the game starts or when spawned
void ADayNightParticle::BeginPlay()
{
	if (Sun)
	{
		Sun->OnChangeLightNightDay.AddDynamic(this, &ADayNightParticle::SetDayNightParticle);
	}


	Super::BeginPlay();
	
}

// Called every frame
void ADayNightParticle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ADayNightParticle::SetDayNightParticle(float Multiplier)
{
	// Reverse Functionality on bReverse
	Multiplier = bReverse ? 1 - Multiplier : Multiplier;

	if (Multiplier <= 0.f)
	{
		DayNightParticleSystem->Deactivate();
	}
	else
	{
		DayNightParticleSystem->Activate();
	}
}

