// Fill out your copyright notice in the Description page of Project Settings.


#include "CollectComplete.h"
#include "Kismet/GameplayStatics.h"
#include "../Character/MainPlayerState.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/AudioComponent.h"


// Sets default values
ACollectComplete::ACollectComplete()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	ParticleSystemComponentLeft = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystemComponentLeft"));
	ParticleSystemComponentRight = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystemComponentRight"));
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));

	RootComponent = AudioComponent;
	ParticleSystemComponentLeft->SetupAttachment(GetRootComponent());
	ParticleSystemComponentRight->SetupAttachment(GetRootComponent());


	ParticleSystemComponentLeft->SetAutoActivate(false);
	ParticleSystemComponentRight->SetAutoActivate(false);
}

// Called when the game starts or when spawned
void ACollectComplete::BeginPlay()
{
	Super::BeginPlay();

	// Add Method to Event
	AMainPlayerState* MainPlayerState = Cast<AMainPlayerState>(UGameplayStatics::GetPlayerController(this, 0)->PlayerState);
	
	if (MainPlayerState)
	{
		MainPlayerState->OnCollectScoreChange.AddDynamic(this, &ACollectComplete::CheckCollectScoreReached);
	}
}

void ACollectComplete::CheckCollectScoreReached(const int& TotalAmount, const int& CurrentAmount)
{
	if (CurrentAmount <= 0)
	{
		ParticleSystemComponentLeft->SetActive(true);
		ParticleSystemComponentRight->SetActive(true);

		if (AudioComponent->Sound)
		{
			AudioComponent->FadeIn(0.3f, AudioComponent->Sound->GetVolumeMultiplier());
		}
	}
}

