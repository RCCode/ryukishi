// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DayNightLight.generated.h"

UCLASS()
class RYUKISHI_API ADayNightLight : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADayNightLight();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Light)
	// Light to change on DayNight Cycle
	class ULocalLightComponent* Light;

	UPROPERTY(EditInstanceOnly, Category = Sun)
	// Pointer to Sun, set in Scene
	TSoftObjectPtr<class ASunMoon> Sun;

	UPROPERTY(EditAnywhere, Category = Light)
	// Reverse functionality
	bool bReverse;

protected:

	UPROPERTY()
	// Intensity set on BeginPlay()
	float MaxIntensity;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void SetupDefaultSubobject();

	virtual void SetupRoot();

	virtual void SetupValues();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	// Set Light Intensity based on Multiplier
	virtual void SetLightIntensity(float Multiplier);

};
