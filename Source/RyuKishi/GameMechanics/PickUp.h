// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "RyuKishi/GameMechanics/Interactable.h"
#include "PickUp.generated.h"

/**
 * 
 */
UCLASS()
class RYUKISHI_API APickUp : public AInteractable
{
	GENERATED_BODY()
	
public:

	APickUp();

	UPROPERTY(EditAnywhere, Category = Stats)
	// If Actor is below this Value, reset to Startposition
	float DeathZ;

	UPROPERTY(EditAnywhere, Category = Stats)
	// Custom TickInterval in Seconds
	float TickInterval;

	UPROPERTY(EditAnywhere, Category = Stats)
	float RelativeDropDistance;

	UPROPERTY(EditAnywhere, Category = Stats)
	// Maximal Distance this Object can be thrown, before respawn
	float MaxThrowDistance;

	UPROPERTY(EditAnywhere, Category = Stats)
	// Offset on respawn after throw
	FVector ThrowRespawnOffset;

	UPROPERTY(EditAnywhere, Category = "Interactable | Mesh")
	// Mesh of Interactable
	class UStaticMeshComponent* Mesh;

	UPROPERTY(EditAnywhere, Category = Sounds)
	class UAudioComponent* HitAudio;

	UPROPERTY(EditAnywhere, Category = Sounds)
	class USoundCue* HitSound;

	UPROPERTY(EditAnywhere, Category = Sounds)
	float ImpulseSize;

private:

	UPROPERTY()
	FVector InitLocation;

	UPROPERTY()
	FVector LastThrowPosition;

private:

	void PlayHitSound();

	void Detach();

	void ResetPhysics();

	void CheckDistanceThrow();

	void ThrowRespawn();

	// Equip this Actor to the player
	void EquipToCharacter(class AMainCharacter* Character);

	// Set the Collision for all CollisionObjects
	void SetCollision(enum ECollisionResponse Response);

protected:

	virtual void SetupDefaultSubobject() override;

	virtual void SetupRoot() override;

	virtual void SetupValues() override;

	virtual void BeginPlay() override;


public:

	virtual void ExecuteInteraction(class AMainCharacter* Character) override;
	
	virtual void Tick(float DeltaTime) override;

	virtual void BeforeInteract(const FRotator& CameraRotation) override;

	virtual void Interact(class AMainCharacter* Character) override;

	UFUNCTION()
	virtual void OnMeshHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);


	// Throw Pickup in Direction of Character
	void Throw(const FVector Direction, const float& Strength);

	// Drop this actor
	void Drop(const FVector Direction);

	UFUNCTION(BlueprintCallable)
	// Check Z Axis for respawn
	void CheckDeathZ();
};
