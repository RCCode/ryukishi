// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Trigger.h"
#include "../EnumCollection.h"
#include "TriggerMusic.generated.h"


/**
 * 
 */
UCLASS()
class RYUKISHI_API ATriggerMusic : public ATrigger
{
	GENERATED_BODY()
	
public:

	ATriggerMusic();

	UPROPERTY(EditAnywhere, Category = Music)
	EMusicStatus MusicTriggerStatus;

protected:

	virtual void SetupValues() override;

public:

	virtual void TriggerSphereOnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void TriggerSphereOnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
};
