// Fill out your copyright notice in the Description page of Project Settings.


#include "DayNightLight.h"
#include "Components/RectLightComponent.h" 
#include "SunMoon.h"


// Sets default values
ADayNightLight::ADayNightLight()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void ADayNightLight::SetupDefaultSubobject()
{
	Light = CreateDefaultSubobject<URectLightComponent>(TEXT("Light"));
}

void ADayNightLight::SetupRoot()
{
	RootComponent = Light;
}

void ADayNightLight::SetupValues()
{
	MaxIntensity = 0;
	bReverse = false;
}


// Called when the game starts or when spawned
void ADayNightLight::BeginPlay()
{
	Super::BeginPlay();
	
	if (Sun != nullptr)
	{
		MaxIntensity = Light->Intensity;
		// Add method to delegate to change the intensity of this light based on the movement of the sun
		Sun->OnChangeLightNightDay.AddDynamic(this, &ADayNightLight::SetLightIntensity);
	}

}

// Called every frame
void ADayNightLight::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADayNightLight::SetLightIntensity(float Multiplier)
{
	// Reverse Functionality on bReverse
	Multiplier = bReverse ? 1 - Multiplier : Multiplier;

	Light->SetIntensity((Multiplier * MaxIntensity));
}

