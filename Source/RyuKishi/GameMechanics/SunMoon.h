// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SunMoon.generated.h"

// Declare Delegate to call if sun goes down or up
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeLightNightDay, float, Multiplier);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSunPositionChange, float, SunAngle);


UCLASS()
class RYUKISHI_API ASunMoon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASunMoon();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Light)
	// Directional Light to represent the Sun
	class UDirectionalLightComponent* Sun;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Light)
	// Directional Light to represent the Moon
	class UDirectionalLightComponent* Moon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Light)
	// Speed to rotate this Actor
	float RotationSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Light)
	// Axis to rotate on
	FRotator RotationVector;

	UPROPERTY(BlueprintAssignable)
	// Delegate value to call if sun goes down or up
	FOnChangeLightNightDay OnChangeLightNightDay;

	UPROPERTY(BlueprintAssignable)
	FOnSunPositionChange OnSunPositionChange;


private:

	UPROPERTY()
	// Current Interpolation Value
	float CurrentInterpValue;

	UPROPERTY()
	float DefaultRotationSpeed;

	UPROPERTY()
	bool bChangeSunRotation;

	UPROPERTY()
	float CurrentTargetAngle;

	UPROPERTY()
	bool bCurrentTargetValue;

private:

	void SetupDefaulSubobjects();

	void SetupRoot();

	void SetupValues();

	// Interpolate to TargetValue and call Delegate
	void InterToTargetValue(const float Target);

	// Sun Rotation is finished
	void SetSunRotationFinished();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void SetSunRotation(float TargetAngle, float SpeedMultiplier = 100.f);

	void ForceSunRotation(const float& NewAngle);

};
