// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../EnumCollection.h"
#include "DayNightMusic.generated.h"

UCLASS()
class RYUKISHI_API ADayNightMusic : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADayNightMusic();

	UPROPERTY(EditInstanceOnly, Category = Sun)
	// Pointer to Sun, set in Scene
	TSoftObjectPtr<class ASunMoon> Sun;

private:

	UPROPERTY()
	class AMainCharacter* Main;

private:

	void SwitchDayNightMusic(const EMusicStatus& Status);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	

	UFUNCTION()
	// Set Music based on Multiplier
	virtual void SetLightMusic(float Multiplier);

};
