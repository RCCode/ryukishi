// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimatedInteractable.h"
#include "RyuKishi/Character/MainCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "Components\SphereComponent.h"


AAnimatedInteractable::AAnimatedInteractable()
{
	SetupDefaultSubobject();
	SetupRoot();
	SetupValues();
}

void AAnimatedInteractable::SetupDefaultSubobject()
{
	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	MeshCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("MeshCollision"));
	InteractAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("InteractAudio"));
	Super::SetupDefaultSubobject();
}

void AAnimatedInteractable::SetupRoot()
{
	RootComponent = SkeletalMesh;
	MeshCollision->SetupAttachment(GetRootComponent());
	InteractAudio->SetupAttachment(CollisionVolume);
	Super::SetupRoot();
}

void AAnimatedInteractable::SetupValues()
{
	SkeletalMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Ignore);
	MeshCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	MeshCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	bIsPlayerFront = true;

	// Set Audio Attenuation
	FSoundAttenuationSettings Set;
	Set.AttenuationShapeExtents.X = 600.0f;
	Set.FalloffDistance = 800.0f;
	InteractAudio->AdjustAttenuation(Set);

	Super::SetupValues();
}

void AAnimatedInteractable::BeginPlay()
{
	Super::BeginPlay();
	MeshCollision->SetBoxExtent(SkeletalMesh->SkeletalMesh->GetBounds().BoxExtent);
}

void AAnimatedInteractable::ExecuteInteraction(AMainCharacter* Character)
{
	bHasInteracted = !bHasInteracted;

	if (bHasInteracted)
	{
		MeshCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		// Calculate if Player is in Front of Object or not
		bIsPlayerFront = (MeshCollision->GetComponentLocation().X - Character->GetActorLocation().X + MeshCollision->GetComponentLocation().Y - Character->GetActorLocation().Y) > 0;
		
		PlayInteractSound(InteractSound);
	}
	else
	{
		MeshCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
		PlayInteractSound(DeInteractSound);
	}
}

void AAnimatedInteractable::PlayInteractSound(class USoundCue*& Sound)
{
	InteractAudio->Stop();
	InteractAudio->Sound = Sound;
	InteractAudio->FadeIn(0.3f, Sound->VolumeMultiplier);
}
