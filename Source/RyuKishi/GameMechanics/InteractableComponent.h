// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "InteractableComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInteractableExecute);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class RYUKISHI_API UInteractableComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInteractableComponent();

	UPROPERTY(BlueprintAssignable)
	// Delegate to call if Component executes Interaction
	FOnInteractableExecute OnInteractableExecute;

	UPROPERTY(EditAnywhere, Category = CollisionVolume)
	// Volume to trigger Events for Interaction
	class USphereComponent* CollisionVolume;

	UPROPERTY(VisibleAnywhere, Category = Widget)
	// UI element to see the hotkey for the player
	class UWidgetComponent* Widget;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Status)
	// Has the player interacted with this object?
	bool bHasInteracted;


private:
	
	void SetupDefaultSubobject();

	void SetupRoot();

	void SetupValues();
	
	// Rotate the widget based on CameraRotation, while the player is looking at this actor
	void SetWidget(const FRotator& CameraRotation);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;


public:	

	// Called if the player is looking at this actor
	void BeforeInteract(const FRotator& CameraRotation);

	// Called on Playerinteraction
	void Interact(class AMainCharacter* Character);

	// Executes before EndInteract()
	void ExecuteInteraction(class AMainCharacter* Character);

	// After ExecuteInteraction() end interaction
	void EndInteract();

	virtual void OnComponentCreated() override;
};
