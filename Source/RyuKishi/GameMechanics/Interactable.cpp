// Fill out your copyright notice in the Description page of Project Settings.


#include "Interactable.h"
#include "Components\StaticMeshComponent.h"
#include "Components\SphereComponent.h"
#include "RyuKishi\GameMechanics\InteractableWidget.h"
#include "Components\WidgetComponent.h"
#include "RyuKishi\Character\MainCharacter.h"

// Sets default values
AInteractable::AInteractable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void AInteractable::SetupDefaultSubobject()
{

	CollisionVolume = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionVolume"));
	Widget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget"));
}

void AInteractable::SetupRoot()
{
	CollisionVolume->SetupAttachment(GetRootComponent());
	Widget->SetupAttachment(CollisionVolume);
}

void AInteractable::SetupValues()
{
	// Set Default Collisionvalues for Linetrace
	CollisionVolume->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	CollisionVolume->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	CollisionVolume->SetUsingAbsoluteScale(true);
	CollisionVolume->SetUsingAbsoluteRotation(true);

	Widget->SetWidgetSpace(EWidgetSpace::World);
	Widget->SetDrawSize(FVector2D(25, 25));
	Widget->SetUsingAbsoluteScale(true);

	bHasInteracted = false;
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();

	if (!Widget->IsRegistered())
	{
		Widget->RegisterComponent();
	}
	Widget->SetVisibility(false);
}


// Called every frame
void AInteractable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AInteractable::BeforeInteract(const FRotator& CameraRotation)
{
	SetWidget(CameraRotation);
}


void AInteractable::SetWidget(const FRotator& CameraRotation)
{
	// Invert Rotation to see UI on the right side

	CollisionVolume->SetWorldRotation(FRotator(-CameraRotation.Pitch, CameraRotation.Yaw - 180, 0));

	if (!Widget->IsVisible())
	{
		Widget->SetVisibility(true);
	}
}

void AInteractable::Interact(AMainCharacter* Character)
{
	ExecuteInteraction(Character);
	EndInteract();
}

void AInteractable::ExecuteInteraction(AMainCharacter* Character)
{
	bHasInteracted = true;
	Character->SetSoundStatus(ESoundStatus::Sound_Interacting);
	// Override Method
}

void AInteractable::EndInteract()
{
	Widget->SetVisibility(false);
}

