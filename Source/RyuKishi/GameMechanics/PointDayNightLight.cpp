// Fill out your copyright notice in the Description page of Project Settings.


#include "PointDayNightLight.h"
#include "Components/PointLightComponent.h"

APointDayNightLight::APointDayNightLight()
{
	SetupDefaultSubobject();
	SetupRoot();
	SetupValues();
}

void APointDayNightLight::SetupDefaultSubobject()
{
	Light = CreateDefaultSubobject<UPointLightComponent>(TEXT("Light"));
}
