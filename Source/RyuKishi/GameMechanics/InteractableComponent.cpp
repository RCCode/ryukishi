// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractableComponent.h"
#include "Components\SphereComponent.h"
#include "Components\WidgetComponent.h"
#include "RyuKishi\Character\MainCharacter.h"

// Sets default values for this component's properties
UInteractableComponent::UInteractableComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	SetupDefaultSubobject();
	SetupRoot();
	SetupValues();
}


void UInteractableComponent::SetupDefaultSubobject()
{
	CollisionVolume = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionVolume"));
	Widget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Widget"));
}

void UInteractableComponent::SetupRoot()
{
	CollisionVolume->SetupAttachment(GetAttachmentRoot());
	Widget->SetupAttachment(CollisionVolume);
}

void UInteractableComponent::SetupValues()
{
	// Set Default Collision values for Linetrace
	CollisionVolume->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	CollisionVolume->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	CollisionVolume->SetUsingAbsoluteScale(true);
	CollisionVolume->SetUsingAbsoluteRotation(true);

	Widget->SetWidgetSpace(EWidgetSpace::World);
	Widget->SetDrawSize(FVector2D(25, 25));
	Widget->SetUsingAbsoluteScale(true);
	Widget->SetRelativeLocation(FVector(CollisionVolume->GetScaledSphereRadius(), 0.f, 0.f));

	bHasInteracted = false;
}

void UInteractableComponent::SetWidget(const FRotator& CameraRotation)
{
	// Invert Rotation to see UI on the right side
	CollisionVolume->SetWorldRotation(FRotator(-CameraRotation.Pitch, CameraRotation.Yaw - 180, 0));

	if (!Widget->IsVisible())
	{
		Widget->SetVisibility(true);
	}
}

// Called when the game starts
void UInteractableComponent::BeginPlay()
{
	Super::BeginPlay();
	if (!Widget->IsRegistered())
	{
		Widget->RegisterComponent();
	}

	Widget->SetVisibility(false);


}

void UInteractableComponent::BeforeInteract(const FRotator& CameraRotation)
{
	if (!bHasInteracted)
	{
		SetWidget(CameraRotation);
	}
}

void UInteractableComponent::Interact(class AMainCharacter* Character)
{
	if (!bHasInteracted)
	{
		ExecuteInteraction(Character);
		EndInteract();
	}
}

void UInteractableComponent::ExecuteInteraction(class AMainCharacter* Character)
{
	bHasInteracted = true;
	Character->SetSoundStatus(ESoundStatus::Sound_Interacting);

	// Broadcast for Parent Actor
	OnInteractableExecute.Broadcast();
}

void UInteractableComponent::EndInteract()
{
	Widget->SetVisibility(false);
}

void UInteractableComponent::OnComponentCreated()
{
	Widget->SetRelativeLocation(FVector(CollisionVolume->GetScaledSphereRadius(), 0.f, 0.f));
	Super::OnComponentCreated();
}
