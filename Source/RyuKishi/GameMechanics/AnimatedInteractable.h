// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "AnimatedInteractable.generated.h"

/**
 * 
 */
UCLASS()
class RYUKISHI_API AAnimatedInteractable : public AInteractable
{
	GENERATED_BODY()
	
public:

	AAnimatedInteractable();

	UPROPERTY(EditAnywhere, Category = "Interactable | Mesh")
	// Skeletal Mesh to animate
	class USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditAnywhere, Category = "Interactable | Mesh")
	// Mesh for blocking Character
	class UBoxComponent* MeshCollision;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Interactable | Status")
	// Check for Player is in front of Object, used in Blueprint
	bool bIsPlayerFront;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Sound)
	class UAudioComponent* InteractAudio;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
	// Play on Characterinteraction
	class USoundCue* InteractSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
	// Play on every second Characterinteraction
	class USoundCue* DeInteractSound;


protected:

	virtual void SetupDefaultSubobject() override;

	virtual void SetupRoot() override;

	virtual void SetupValues() override;

	virtual void BeginPlay() override;

	void PlayInteractSound(class USoundCue*& Sound);

public:

	virtual void ExecuteInteraction(class AMainCharacter* Character) override;
};
