// Fill out your copyright notice in the Description page of Project Settings.


#include "TriggerMusic.h"
#include "../Character/MainCharacter.h"

ATriggerMusic::ATriggerMusic()
{
	SetupAttachment();
	SetupRoot();
	SetupValues();
}


void ATriggerMusic::SetupValues()
{
	Super::SetupValues();
	MusicTriggerStatus = EMusicStatus::Music_Default;
}


void ATriggerMusic::TriggerSphereOnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		AMainCharacter* Player = Cast<AMainCharacter>(OtherActor);
		if (Player && MusicTriggerStatus != EMusicStatus::Music_Default)
		{
			Player->SetMusicStatus(MusicTriggerStatus);
		}
	}
}

void ATriggerMusic::TriggerSphereOnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor)
	{
		AMainCharacter* Player = Cast<AMainCharacter>(OtherActor);
		if (Player && MusicTriggerStatus != EMusicStatus::Music_Default)
		{
			Player->SetMusicStatus(EMusicStatus::Music_Main);
		}
	}
}
