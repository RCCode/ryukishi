// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "InteractableAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class RYUKISHI_API UInteractableAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BlueprintValues)
	class AAnimatedInteractable* AnimatedInteractable;

	virtual void NativeInitializeAnimation() override;

};
