// Fill out your copyright notice in the Description page of Project Settings.


#include "Collectable.h"
#include "Kismet/GameplayStatics.h" 
#include "Particles\ParticleSystemComponent.h"
#include "Components\SphereComponent.h"
#include "../Character/MainPlayerState.h"

ACollectable::ACollectable()
{
	SetupDefaultSubobject();
	SetupRoot();
	SetupValues();
}

void ACollectable::SetupDefaultSubobject()
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	InteractParticles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("InteractParticles"));
	Super::SetupDefaultSubobject();
}

void ACollectable::SetupRoot()
{
	RootComponent = Mesh;
	InteractParticles->SetupAttachment(GetRootComponent());
	Super::SetupRoot();
}

void ACollectable::SetupValues()
{
	Mesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Ignore);
	InteractParticles->SetAutoActivate(false);
	Super::SetupValues();
}

void ACollectable::BeginPlay()
{
	Super::BeginPlay();
	InteractParticles->OnSystemFinished.AddDynamic(this, &ACollectable::EndCollectable);

	MainPlayerState = Cast<AMainPlayerState>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->PlayerState);

	if (MainPlayerState)
	{
		MainPlayerState->IncrementCollectables();
	}

}


void ACollectable::BeforeInteract(const FRotator& CameraRotation)
{
	if (!bHasInteracted)
	{
		Super::BeforeInteract(CameraRotation);
	}
}

void ACollectable::Interact(class AMainCharacter* Character)
{
	if (!bHasInteracted)
	{
		Super::Interact(Character);
	}
}

void ACollectable::ExecuteInteraction(class AMainCharacter* Character)
{
	Super::ExecuteInteraction(Character);

	if (InteractParticles)
	{
		InteractParticles->Activate();
	}

	if (MainPlayerState)
	{
		MainPlayerState->DecrementCollectables();
	}

}

void ACollectable::EndCollectable(UParticleSystemComponent* PSystem)
{
	Destroy();
}
