// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Navigation/PathFollowingComponent.h"
#include "CompanionCharacter.generated.h"

UENUM(BlueprintType)
enum class ECompanionStatus : uint8
{
	CS_Idle				UMETA(DisplayName = "Idle"),
	CS_MoveToTarget		UMETA(DisplayName = "MoveToTarget"),
	CS_Default			UMETA(DisplayName = "Default")
};

UCLASS()
class RYUKISHI_API ACompanionCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACompanionCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Status)
	ECompanionStatus CompanionStatus;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = AI)
	class USphereComponent* ReactionSphere;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Interaction)
	class UInteractableComponent* InteractableComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Interaction)
	class UParticleSystemComponent* InteractionParticles; 

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = AI)
	class AAIController* AIController;

	UPROPERTY(EditAnywhere, Category = AI)
	float AcceptanceRadius;

	UPROPERTY(EditAnywhere, Category = DistanceToPlayer)
	// If Player is more than Distance, move AI faster than Walkingspeed of Player
	float FarDistance;

	UPROPERTY(EditAnywhere, Category = DistanceToPlayer)
	// If Player is more than Distance, move AI at the same Speed as Player
	float NormalDistance;

	UPROPERTY(EditAnywhere, Category = DistanceToPlayer)
	float JumpZLimit;

	UPROPERTY(EditAnywhere, Category = Animation)
	float IdleDelay;

	UPROPERTY(EditDefaultsOnly, Category = Animation)
	TArray<FName> IdleMontageNames;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation)
	class UAnimMontage* IdleMontage;

	UPROPERTY(EditAnywhere, Category = Jump)
	float MinDifferenceToPlayerForJump;

	UPROPERTY(EditAnywhere, Category = Jump)
	float MaxDifferenceToPlayerForJump;

	UPROPERTY(EditAnywhere, Category = ReJump)
	float DeathZ;

	UPROPERTY(EditAnywhere, Category = ReJump)
	FVector ReJumpDirection;

	UPROPERTY(EditAnywhere, Category = ReJump)
	float ReJumpForce;


private:

	UPROPERTY()
	class AMainCharacter* Player;

	UPROPERTY()
	float DistanceToPlayer;

	UPROPERTY()
	float ZDifferenceToPlayer;

	UPROPERTY()
	float LastDistanceToPlayer;

	UPROPERTY()
	class UAnimInstance* AnimInstance;

	UPROPERTY()
	bool bIdleAnimationFinished;

	UPROPERTY()
	bool bCanJump;

	UPROPERTY()
	int StartTimer;

	UPROPERTY()
	float DefaultJumpZVelocity;

	UPROPERTY()
	FVector LastGroundPosition;

	UPROPERTY()
	bool bWasInAir;

private:

	// Create DefaultSubobjects
	void SetupDefaultSubobject();

	// Set Root for Subobjects
	void SetupRoot();

	// Set default Values for Subobjects
	void SetupValues();

	void SetWalkSpeed(float& MaxWalkSpeed);

	void CheckDistance();

	// Behavior for AI in Air
	void InAirBehavior();

	// Check if AI needs to Jump, to reach Player
	bool CheckForJump();

	void CheckDeathZ();

	void ReJump();

	void PlayIdleAnimation();

	bool Delay(float& DelaySeconds);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UFUNCTION()
	virtual void OnInteraction();

	UFUNCTION()
	virtual void ReactionSphereOnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	virtual void ReactionSphereOnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	virtual void OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result);

	UFUNCTION(BlueprintCallable)
	void MoveToTarget(class AMainCharacter* Target);

	UFUNCTION(BlueprintCallable)
	void IdleAnimationFinished();

	virtual void OutsideWorldBounds() override;


	FORCEINLINE ECompanionStatus GetCompanionStatus() { return CompanionStatus; }
	FORCEINLINE void SetCompanionStatus(ECompanionStatus Status) { CompanionStatus = Status; }

};
