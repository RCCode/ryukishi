// Fill out your copyright notice in the Description page of Project Settings.


#include "MainCharacter.h"
#include "GameFramework\SpringArmComponent.h"
#include "Camera\CameraComponent.h"
#include "Components\CapsuleComponent.h"
#include "GameFramework\CharacterMovementComponent.h"
#include "RyuKishi\GameMechanics\Interactable.h"
#include "Kismet\KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "RyuKishi\GameMechanics\PickUp.h"
#include "Animation\AnimInstance.h"
#include "Engine/Canvas.h" 
#include "Components\SphereComponent.h"
#include "Components/AudioComponent.h"
#include "AudioDevice.h" 
#include "MainPlayerController.h"
#include "../GameMechanics/SunMoon.h"
#include "GameFramework/GameModeBase.h" 
#include "../GameMechanics/InteractableComponent.h"
#include "CompanionCharacter.h"


// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetupDefaultSubobject();
	SetupRoot();
	SetupValues();
}

#pragma region private

void AMainCharacter::SetupDefaultSubobject()
{
	CameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	CameraToFollow = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraToFollow"));
	BackgroundMusic = CreateDefaultSubobject<UAudioComponent>(TEXT("BackgroundMusic"));
	SFXGround = CreateDefaultSubobject<UAudioComponent>(TEXT("SFXGround"));
}

void AMainCharacter::SetupRoot()
{
	CameraSpringArm->SetupAttachment(GetRootComponent());

	// Attach the Camera to the defined Position from the SpringarmComponent
	CameraToFollow->SetupAttachment(CameraSpringArm, USpringArmComponent::SocketName);

	BackgroundMusic->SetupAttachment(GetRootComponent());
	SFXGround->SetupAttachment(GetRootComponent());
}

void AMainCharacter::SetupValues()
{
	// Configure Camera
	CameraSpringArm->TargetArmLength = 400;
	ZoomTargetValue = CameraSpringArm->TargetArmLength;
	CameraSpringArm->TargetOffset.Z = 80;
	CameraSpringArm->bEnableCameraLag = true;
	CameraSpringArm->bEnableCameraRotationLag = true;
	CameraSpringArm->CameraLagSpeed = 50.f;
	CameraSpringArm->CameraRotationLagSpeed = 50.f;
	CameraSpringArm->CameraLagMaxDistance = 10.f;
	CameraSpringArm->bUsePawnControlRotation = true;

	CameraToFollow->bUsePawnControlRotation = false;
	BaseTurnRate = 65;
	BaseLookUpRate = 65;

	// Set CollisionCapsule
	GetCapsuleComponent()->SetCapsuleSize(34, 88);

	// Do not rotate with Controller
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	// Configure CharacterMovement
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 320.f, 0.f);
	GetCharacterMovement()->JumpZVelocity = 300;
	GetCharacterMovement()->AirControl = 0.8;

	WalkingSpeed = 300;
	SprintingSpeed = 600;

	// Configure Interaction
	InteractRange = 180;
	bInteractKeyPressed = false;
	bThrowKeyPressed = false;
	bDropKeyPressed = false;
	bPauseKeyPressed = false;
	bSitKeyPressed = false;
	bIsSitting = false;

	CurrentInteraction = nullptr;
	CurrentComponentInteraction = nullptr;
	InteractionStartOffset = FVector(0.f, 0.f, 75.f);
	CurrentPickup = nullptr;
	ThrowStrength = 3000;
	ThrowAngle = FVector(0.f, 0.f, 4.f);

	SetAnimationStatus(EAnimationStatus::EAS_Default);

	bEnableDebugRays = false;
	bDevControls = true;

	// Configure Zoom
	MaxZoom = 500.f;
	MinZoom = 100.f;

	ZoomAmount = 50;
	ZoomInterpSpeed = 2;
	
	// Configure Sprint Interpolation
	SprintTargetValue = 0;
	SprintAmount = 50;
	SprintInterpSpeed = 2;

	// Configure Music
	BackgroundMusic->bIsMusic = true;
	SFXGround->bOverrideAttenuation = true;
	MusicStatus = EMusicStatus::Music_Default;
	OverlappedMusicCounter = 0;
	MusicTimeStamps.Init(0.f, int(EMusicStatus::Music_Default) + 1);

	MusicFadInDuration = 1.f;
	MusicFadOutDuration = 1.5f;

	// Configure ReJump
	DeathZ = -10000;
	ReJumpDirection = FVector(0.f, 0.f, 5.f);
	ReJumpForce = 100.f;

	TutorialStatus = ETutorialStatus::T_Press_E;
}

void AMainCharacter::BindInputAction(UInputComponent* PlayerInputComponent)
{
	// Jump
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AMainCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Move Fast
	PlayerInputComponent->BindAction("MoveFast", IE_Pressed, this, &AMainCharacter::MoveFast);
	PlayerInputComponent->BindAction("MoveFast", IE_Released, this, &AMainCharacter::StopMoveFast);

	// Interact
	PlayerInputComponent->BindAction("Interaction", IE_Pressed, this, &AMainCharacter::InteractionPressed);
	PlayerInputComponent->BindAction("Interaction", IE_Released, this, &AMainCharacter::InteractionReleased);

	// Throw
	PlayerInputComponent->BindAction("Throw", IE_Pressed, this, &AMainCharacter::ThrowPressed);
	PlayerInputComponent->BindAction("Throw", IE_Released, this, &AMainCharacter::ThrowReleased);

	// Pause
	PlayerInputComponent->BindAction("ESC", IE_Pressed, this, &AMainCharacter::PausePressed).bExecuteWhenPaused = true;
	PlayerInputComponent->BindAction("ESC", IE_Released, this, &AMainCharacter::PauseReleased).bExecuteWhenPaused = true;

	//Sit
	PlayerInputComponent->BindAction("Sit", IE_Pressed, this, &AMainCharacter::SitPressed);
	PlayerInputComponent->BindAction("Sit", IE_Released, this, &AMainCharacter::SitReleased);

}

void AMainCharacter::BindInputAxis(UInputComponent* PlayerInputComponent)
{
	// Move
	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainCharacter::MoveRight);

	// Turning Camera with Mouse
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	// Turning Camera with Keyboard
	PlayerInputComponent->BindAxis("TurnRate", this, &AMainCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMainCharacter::LookUpRate);

	// Zoom
	PlayerInputComponent->BindAxis("Zoom", this, &AMainCharacter::Zoom);

	if (bDevControls)
	{
		PlayerInputComponent->BindAxis("Zoom", this, &AMainCharacter::ChangeSunRotation).bExecuteWhenPaused = true;
	}
}

void AMainCharacter::InteractLineTrace()
{
	// Keep static Values for LineTrace, because it is running every Frame
	static FHitResult OutHit;
	static FCollisionQueryParams CollisionParams;
	static FVector Start;
	static FVector ForwardVector;
	static FVector End;

	Start = GetActorLocation() + InteractionStartOffset;
	ForwardVector = CameraToFollow->GetForwardVector();
	End = ((ForwardVector * InteractRange) + Start);

	if (bEnableDebugRays)
	{
		UKismetSystemLibrary::DrawDebugArrow(this, Start, End, 10.f, FLinearColor::Green, 5, 5.f);
	}

	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECC_Visibility, CollisionParams))
	{
		if (OutHit.bBlockingHit)
		{
			// Cast to interactable Item
			if (!StartInteraction(Cast<AInteractable>(OutHit.Actor.Get())))
			{
				StartInteraction(Cast<UInteractableComponent>(OutHit.GetComponent()->GetAttachParent()));
			}
		}
	}
	else
	{
		EndInteraction();
	}
}


bool AMainCharacter::StartInteraction(AInteractable* Interaction)
{
	// Check if Cast is not null
	if (!Interaction || 
	// Check for multiple Interaction targets
	(CurrentInteraction && Interaction != CurrentInteraction))
	{
		EndInteraction();
		return false;
	}

	CurrentInteraction = Interaction;

	// Check if Player pressed Interact Key
	if (bInteractKeyPressed)
	{
		if(bIsSitting) PlaySitMontage();

		// Check for PickUp switch
		if (CurrentPickup)
		{
			if (CheckIsPickUp(Interaction))
			{
				CurrentPickup->Drop(GetActorForwardVector());
				if (CurrentInteraction != CurrentPickup)
				{
					SetCurrentPickUp(Interaction);
				}
				else
				{
					// If Interaction is the one the Character is holding
					SetSoundStatus(ESoundStatus::Sound_CancelInteracting);
					CurrentPickup = nullptr;
					bInteractKeyPressed = false;
					EndInteraction();
					return false;
				}
			}
		}
		else
		{
			if (SetCurrentPickUp(Interaction))
			{
				if ((int)TutorialStatus)
				{
					ChangeTutorial(ETutorialStatus::T_LMB_Throw);
					ChangeTutorial(ETutorialStatus::T_E_Drop);
				}
			}
		}

		// Do not move while Interacting
		SetMovementSpeed();

		// Rotate Character to Interactable
		FRotator LookAtRotation(0, (Interaction->CollisionVolume->GetComponentLocation() - GetActorLocation()).Rotation().Yaw, 0);
		SetActorRotation(LookAtRotation);

		SetAnimationStatus(EAnimationStatus::EAS_Interacting);

		// After Cast call method to interact
		Interaction->Interact(this);

		// Interact once per Key press
		bInteractKeyPressed = false;
	}
	else
	{
		ChangeTutorial(ETutorialStatus::T_Press_E);
		// Show Input icon before Interaction
		Interaction->BeforeInteract(CameraToFollow->GetComponentRotation());
	}

	return true;
}

void AMainCharacter::StartInteraction(class UInteractableComponent* Interaction)
{
	if (!Interaction ||
		// Check for multiple Interaction targets
		(CurrentComponentInteraction && Interaction != CurrentComponentInteraction))
	{
		EndInteraction();
		return;
	}

	CurrentComponentInteraction = Interaction;

	// Check if Player pressed Interact Key
	if (bInteractKeyPressed)
	{
		if(bIsSitting) PlaySitMontage();

		// Do not move while Interacting
		SetMovementSpeed();

		// Rotate Character to Interactable
		FRotator LookAtRotation(0, (Interaction->CollisionVolume->GetComponentLocation() - GetActorLocation()).Rotation().Yaw, 0);
		SetActorRotation(LookAtRotation);

		SetAnimationStatus(EAnimationStatus::EAS_Interacting);

		// After Cast call method to interact
		Interaction->Interact(this);

		// Interact once per Key press
		bInteractKeyPressed = false;
	}
	else
	{
		ChangeTutorial(ETutorialStatus::T_Press_E);
		// Show Input icon before Interaction
		Interaction->BeforeInteract(CameraToFollow->GetComponentRotation());
	}
}

void AMainCharacter::EndInteraction()
{
	// Check if CurrentInteraction is not null
	if (CurrentInteraction || CurrentComponentInteraction)
	{
		if (CurrentInteraction)
		{
			CurrentInteraction->EndInteract();
			CurrentInteraction = nullptr;
		}
		if (CurrentComponentInteraction)
		{
			CurrentComponentInteraction->EndInteract();
			CurrentComponentInteraction = nullptr;
		}

		if (!CheckIsNotAnim(EAnimationStatus::EAS_Interacting))
		{
			SetAnimationStatus(EAnimationStatus::EAS_Default);
		}
	}

}

void AMainCharacter::SetMovementSpeed()
{
	GetCharacterMovement()->MaxWalkSpeed = 0;
}

void AMainCharacter::SetMovementSpeed(const float& Speed)
{
	GetCharacterMovement()->MaxWalkSpeed = Speed;
}

class APickUp* AMainCharacter::CheckIsPickUp(class AInteractable* Interactable)
{
	return Cast<APickUp>(Interactable);
}

bool AMainCharacter::SetCurrentPickUp(AInteractable* Interactable)
{
	CurrentPickup = CheckIsPickUp(Interactable);
	return CurrentPickup != nullptr;
}


void AMainCharacter::SwitchMusicStatus(const EMusicStatus& Status)
{
	switch (Status)
	{
	case EMusicStatus::Music_Main:
		if (MainMusic)
			SetBackgroundMusic(MainMusic);
		break;
	case EMusicStatus::Music_Day:
		if (MainMusic)
			SetBackgroundMusic(MainMusic);
		break;
	case EMusicStatus::Music_Night:
		if (NightMusic)
			SetBackgroundMusic(NightMusic);
		break;
	case EMusicStatus::Music_Forest:
		if (ForestMusic)
			SetBackgroundMusic(ForestMusic);
		break;
	case EMusicStatus::Music_Shrine:
		if (ShrineMusic)
			SetBackgroundMusic(ShrineMusic);
		break;
	case EMusicStatus::Music_Dragon:
		if (DragonMusic)
			SetBackgroundMusic(DragonMusic);
		break;
	case EMusicStatus::Music_Default:
		if (MainMusic)
			SetBackgroundMusic(MainMusic);
		break;
	default:
		break;
	}
}

void AMainCharacter::SetBackgroundMusic(class USoundCue*& Sound)
{
	if (BackgroundMusic->Sound == nullptr)
	{
		BackgroundMusic->Stop();
		BackgroundMusic->Sound = Sound;
		BackgroundMusic->FadeIn(MusicFadInDuration, Sound->VolumeMultiplier, MusicTimeStamps[int(MusicStatus)] * Sound->FirstNode->ChildNodes[0]->GetDuration());
	}
	else
	{
		BackgroundMusic->FadeOut(MusicFadOutDuration, 0.f);
	}
}

void AMainCharacter::FadeInBackgroundMusic()
{
	if (BackgroundMusic->bIsFadingOut && !BackgroundMusic->IsActive())
	{
		BackgroundMusic->Sound = nullptr;
		SwitchMusicStatus(MusicStatus);
	}
}

void AMainCharacter::SetSFXSound(class USoundCue*& Sound)
{
	SFXGround->Stop();
	SFXGround->Sound = Sound;
	SFXGround->FadeIn(0.4f, Sound->VolumeMultiplier);
}

void AMainCharacter::MoveCharacter(const float& Value, const EAxis::Type& DirectionAxis)
{
	if ((Value != 0.0f))
	{
		// Always check inherited values
		if (!Controller || !CheckIsNotAnim(EAnimationStatus::EAS_Throwing))
		{
			return;
		}

		if (bIsSitting)
		{
			PlaySitMontage();
			return;
		}

		// Get Forward Vector
		const FVector Direction =
			FRotationMatrix
			(
				FRotator(0, Controller->GetControlRotation().Yaw, 0)
			)
			.GetUnitAxis(DirectionAxis);

		if (CheckIsNotAnim(EAnimationStatus::EAS_Running))
		{
			SetAnimationStatus(EAnimationStatus::EAS_Walking);
			SetMovementSpeed(WalkingSpeed);
			SprintTargetValue = WalkingSpeed;
			if (SoundStatus != ESoundStatus::Sound_Jumping && SoundStatus != ESoundStatus::Sound_Landing && SoundStatus != ESoundStatus::Sound_Running)
			{
				SetSoundStatus(ESoundStatus::Sound_Walking);
			}
		}
		else
		{
			if (SoundStatus != ESoundStatus::Sound_Jumping && SoundStatus != ESoundStatus::Sound_Landing && SoundStatus != ESoundStatus::Sound_Running)
			{
				SetSoundStatus(ESoundStatus::Sound_Running);
			}
		}

		if (CheckIsNotAnim(EAnimationStatus::EAS_Interacting))
		{
			AddMovementInput(Direction, Value);
		}

		InterpSprintingSpeed();
	}
	else
	{
		if (GetVelocity().Size() <= 0)
		{
			SetMovementSpeed(WalkingSpeed);
			if ((SoundStatus == ESoundStatus::Sound_Walking || SoundStatus == ESoundStatus::Sound_Running) || !SFXGround->IsPlaying())
			{
				if (SoundStatus != ESoundStatus::Sound_Jumping)
				{
					SetSoundStatus(ESoundStatus::Sound_Idle);
				}
			}
		}
	}
}

void AMainCharacter::InterpSprintingSpeed()
{
	// Set MovementSpeed for Sprint speed by Interpolation (Only AxisBinding are called every Frame)
	if (!CheckIsNotAnim(EAnimationStatus::EAS_Running) && !FMath::IsNearlyEqual(SprintTargetValue, GetCharacterMovement()->MaxWalkSpeed, 1))
	{
		SetMovementSpeed(FMath::InterpExpoOut(GetCharacterMovement()->MaxWalkSpeed, SprintTargetValue, SprintInterpSpeed * GetWorld()->GetDeltaSeconds()));
	}
}

void AMainCharacter::CheckDeathZ()
{
	if (GetCharacterMovement()->IsFalling())
	{
		if (GetActorLocation().Z < DeathZ)
		{
			ReJump();
		}
	}
	else
	{
		LastGroundPosition = GetActorLocation();
	}
}

void AMainCharacter::ReJump()
{
	// Reset Velocity & Accelation
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_None);
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);

	// Add Impulse to Last Ground Location
	GetCharacterMovement()->AddImpulse(((LastGroundPosition - GetActorLocation()) + ReJumpDirection) * ReJumpForce);
}

void AMainCharacter::PlaySitMontage()
{
	
	if (CheckIsNotAnim(EAnimationStatus::EAS_Throwing) && !GetCharacterMovement()->IsFalling())
	{
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance && SitMontage)
		{
			if (!bIsSitting)
			{

				if (CurrentPickup)
				{
					AnimInstance->Montage_Play(SitMontage);
					AnimInstance->Montage_JumpToSectionsEnd(FName("SitItem"), SitMontage);
				}
				else
				{
					AnimInstance->Montage_Play(SitMontage);
					AnimInstance->Montage_JumpToSectionsEnd(FName("Sit"), SitMontage);
				}

				CameraSpringArm->TargetOffset /= 4;
			}
			else
			{
				AnimInstance->Montage_Play(SitMontage, -4.f);
				AnimInstance->Montage_JumpToSectionsEnd(FName("Stand"), SitMontage);
				CameraSpringArm->TargetOffset *= 4;
			}

			SetAnimationStatus(EAnimationStatus::EAS_Default);
			bIsSitting = !bIsSitting;
		}
	}
}

void AMainCharacter::ChangeTutorial(const ETutorialStatus& SelectedStatus)
{

	if (!(int)TutorialStatus || SelectedStatus != TutorialStatus)
	{
		return;
	}
	
	switch (TutorialStatus)
	{
	case ETutorialStatus::T_Press_E:
		MainPlayerController->ChangeMessageText(FString("Tippe E zum interagieren"));
		break;
	case ETutorialStatus::T_LMB_Throw:
		MainPlayerController->ChangeMessageText(FString("Nutze LMB zum werfen"));
		break;
	case ETutorialStatus::T_LMB_Hold:
		MainPlayerController->ChangeMessageText(FString("Halte LMB zum zielen"));
		break;
	case ETutorialStatus::T_E_Drop:
		MainPlayerController->ChangeMessageText(FString("Nutze E um fallen zu lassen"));
		break;
	case ETutorialStatus::T_Default:
		TutorialStatus = ETutorialStatus::T_Finished;
		break;
	default:
		break;
	}

	TutorialStatus = (ETutorialStatus)((int)TutorialStatus + 1);

}

#pragma endregion

#pragma region protected

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	BackgroundMusic->OnAudioPlaybackPercent.AddDynamic(this, &AMainCharacter::OnMusicPlayBackPercent);
	SetMusicStatus(EMusicStatus::Music_Main);
	MainPlayerController = Cast<AMainPlayerController>(GetController());
}


#pragma endregion

#pragma region public

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	InteractLineTrace();
	FadeInBackgroundMusic();
	CheckDeathZ();
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Always check inherited values
	check(PlayerInputComponent);

	BindInputAction(PlayerInputComponent);
	BindInputAxis(PlayerInputComponent);
}

void AMainCharacter::Jump()
{
	if (CheckIsNotAnim(EAnimationStatus::EAS_Throwing))
	{
		if (bIsSitting)
		{
			PlaySitMontage();
			return;
		}

		Super::Jump();
		SetSoundStatus(ESoundStatus::Sound_Jumping);
	}
}

void AMainCharacter::MoveForward(float Value)
{
	MoveCharacter(Value, EAxis::X);
}

void AMainCharacter::MoveRight(float Value)
{
	MoveCharacter(Value, EAxis::Y);
}

void AMainCharacter::MoveFast()
{
	if (CheckIsNotAnim(TArray<EAnimationStatus> { EAnimationStatus::EAS_Interacting, EAnimationStatus::EAS_Throwing }))
	{
		SetAnimationStatus(EAnimationStatus::EAS_Running);
		SprintTargetValue = SprintingSpeed;
		if (GetCharacterMovement()->Velocity.Size2D() > 0)
		{
			SetSoundStatus(ESoundStatus::Sound_Running);
		}
	}
}

void AMainCharacter::StopMoveFast()
{
	if (CheckIsNotAnim(TArray<EAnimationStatus> { EAnimationStatus::EAS_Interacting, EAnimationStatus::EAS_Throwing }))
	{
		SetAnimationStatus(EAnimationStatus::EAS_Walking);
		SetMovementSpeed(WalkingSpeed);
		SprintTargetValue = WalkingSpeed;
		SetSoundStatus(ESoundStatus::Sound_Walking);
	}
}

void AMainCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::LookUpRate(float Rate)
{
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::Zoom(float Rate)
{
	if (CameraSpringArm)
	{
		// Set TargetValue
		if (Rate != 0)
		{
			ZoomTargetValue += Rate * ZoomAmount;
			ZoomTargetValue = FMath::Clamp(ZoomTargetValue, MinZoom, MaxZoom);
		}
		// Interpolate to TargetValue
		if (!FMath::IsNearlyEqual(ZoomTargetValue, CameraSpringArm->TargetArmLength, 1))
		{
			CameraSpringArm->TargetArmLength = FMath::FInterpTo(CameraSpringArm->TargetArmLength, ZoomTargetValue, GetWorld()->GetDeltaSeconds(), ZoomInterpSpeed);
		}
	}
}

void AMainCharacter::ChangeSunRotation(float Rate)
{
	if (bDevControls)
	{
		if (MainPlayerController->bPauseMenuVisible && MainPlayerController->Sun)
		{
			if (Rate != 0)
			{
				MainPlayerController->Sun->ForceSunRotation(Rate * 5.f);
			}
		}
	}
}

void AMainCharacter::InteractionPressed()
{
	if (CurrentPickup && !CurrentInteraction)
	{
		if (bIsSitting)
		{
			PlaySitMontage();
		}

		// Second Key for canceling Throwing
		if (!CheckIsNotAnim(EAnimationStatus::EAS_Throwing))
		{
			UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
			if (AnimInstance && ThrowMontage)
			{
				AnimInstance->Montage_Play(ThrowMontage);
				AnimInstance->Montage_JumpToSectionsEnd(FName("Release"), ThrowMontage);
				SetAnimationStatus(EAnimationStatus::EAS_Default);
				bUseControllerRotationYaw = false;
			}
		}
		else
		{
			CurrentPickup->Drop(GetActorForwardVector());
			bDropKeyPressed = true;
			SetSoundStatus(ESoundStatus::Sound_CancelInteracting);
		}
	}
	else
	{
		bInteractKeyPressed = true;
	}
}

void AMainCharacter::InteractionReleased()
{
	if (bDropKeyPressed)
	{
		bDropKeyPressed = false;
		CurrentPickup = nullptr;
	}
	bInteractKeyPressed = false;

	if (!CheckIsNotAnim(EAnimationStatus::EAS_Interacting))
	{
		SetAnimationStatus(EAnimationStatus::EAS_Default);
	}
}

void AMainCharacter::ThrowPressed()
{
	bThrowKeyPressed = true;

	if (CurrentPickup)
	{
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (CheckIsNotAnim(EAnimationStatus::EAS_Throwing) && AnimInstance && ThrowMontage)
		{
			ChangeTutorial(ETutorialStatus::T_LMB_Hold);
			AnimInstance->Montage_Play(ThrowMontage);
			AnimInstance->Montage_JumpToSection(FName("Prepare"), ThrowMontage);
			SetAnimationStatus(EAnimationStatus::EAS_Throwing);
			SetSoundStatus(ESoundStatus::Sound_Idle);
			bUseControllerRotationYaw = true;
		}
	}
}

void AMainCharacter::ThrowReleased()
{

	bThrowKeyPressed = false;

	if (CurrentPickup && !CheckIsNotAnim(EAnimationStatus::EAS_Throwing))
	{

		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance && ThrowMontage)
		{
			AnimInstance->Montage_Play(ThrowMontage);
			AnimInstance->Montage_JumpToSection(FName("Release"), ThrowMontage);
		}
	}
}

void AMainCharacter::PausePressed()
{
	bPauseKeyPressed = true;
	if (MainPlayerController)
	{
		MainPlayerController->TogglePauseMenu();
	}
}

void AMainCharacter::PauseReleased()
{
	bPauseKeyPressed = false;
}

void AMainCharacter::SitPressed()
{
	bSitKeyPressed = true;

	PlaySitMontage();
}

void AMainCharacter::SitReleased()
{
	bSitKeyPressed = false;
}

void AMainCharacter::ThrowEventObjectReleased()
{
	CurrentPickup->Throw(GetActorForwardVector() + ThrowAngle, ThrowStrength);
	CurrentPickup = nullptr;
	SetAnimationStatus(EAnimationStatus::EAS_Default);
	bUseControllerRotationYaw = false;
	SetSoundStatus(ESoundStatus::Sound_Throwing);

}

void AMainCharacter::SetAnimationStatus(const EAnimationStatus& Status)
{
	AnimationStatus = Status;
}

void AMainCharacter::SetSoundStatus(const ESoundStatus& Status)
{
	if (SoundStatus != Status && !GetCharacterMovement()->IsFalling())
	{
		switch (Status)
		{
		case ESoundStatus::Sound_Walking:
			if (WalkingSound)
				SetSFXSound(WalkingSound);
			break;
		case ESoundStatus::Sound_Running:
			if (RunningSound)
				SetSFXSound(RunningSound);
			break;
		case ESoundStatus::Sound_Interacting:
			if (InteractingSound)
				UGameplayStatics::PlaySoundAtLocation(this, InteractingSound, GetActorLocation());
			break;
		case ESoundStatus::Sound_CancelInteracting:
			if (InteractingSound)
				UGameplayStatics::PlaySoundAtLocation(this, InteractingSound, GetActorLocation(), InteractingSound->VolumeMultiplier, 0.5f);
			break;
		case ESoundStatus::Sound_Throwing:
			if (ThrowingSound)
				UGameplayStatics::PlaySoundAtLocation(this, ThrowingSound, GetActorLocation());
			break;
		case ESoundStatus::Sound_Jumping:
			if (JumpSound)
				SetSFXSound(JumpSound);
			break;
		case ESoundStatus::Sound_Landing:
			if (LandSound)
				SetSFXSound(LandSound);
			break;
		case ESoundStatus::Sound_Idle:
			SFXGround->Stop();
			SFXGround->Sound = nullptr;
			break;
		default:
			break;
		}
		SoundStatus = Status;
	}
}

void AMainCharacter::SetMusicStatus(const EMusicStatus& Status)
{
	if (MusicStatus != EMusicStatus::Music_Night || Status == EMusicStatus::Music_Day)
	{
		if (Status != EMusicStatus::Music_Main && Status != EMusicStatus::Music_Night && Status != EMusicStatus::Music_Day)
		{
			OverlappedMusicCounter++;
		}
		else if ((OverlappedMusicCounter > 0))
		{
			OverlappedMusicCounter--;
			if (OverlappedMusicCounter > 0)
			{
				return;
			}
		}

		if (MusicStatus != Status)
		{
			SwitchMusicStatus(Status);
			MusicStatus = Status;
		}
	}

}

bool AMainCharacter::CheckIsNotAnim(const EAnimationStatus& IsNotStatus)
{
	return AnimationStatus != IsNotStatus;
}

bool AMainCharacter::CheckIsNotAnim(const TArray<EAnimationStatus>& IsNotStatus)
{
	for (int i = 0; i < IsNotStatus.Num(); i++)
	{
		if (AnimationStatus == IsNotStatus[i])
		{
			return false;
		}
	}

	return true;
}

void AMainCharacter::OnMusicPlayBackPercent(const class USoundWave* PlayingSoundWave, const float PlaybackPercent)
{
	if (PlayingSoundWave && BackgroundMusic->Sound && !BackgroundMusic->bIsFadingOut && BackgroundMusic->VolumeMultiplier >= 1.f)
	{
		MusicTimeStamps[(int)MusicStatus] = PlaybackPercent;
		if (PlaybackPercent >= 1.f)
		{
			FAudioThread::RunCommandOnAudioThread([this]()
			{
				//Code will run in Audio Thread, not in Game Thread because of FindActiveSound()
				BackgroundMusic->GetAudioDevice()->FindActiveSound(BackgroundMusic->GetAudioComponentID())->RequestedStartTime = 0;
			});

		}
	}
}

void AMainCharacter::OutsideWorldBounds()
{
	MainPlayerController->RestartLevel();
}

#pragma endregion
