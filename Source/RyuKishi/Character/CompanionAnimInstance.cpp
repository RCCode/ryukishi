// Fill out your copyright notice in the Description page of Project Settings.


#include "CompanionAnimInstance.h"
#include "GameFramework\CharacterMovementComponent.h"
#include "CompanionCharacter.h"

void UCompanionAnimInstance::NativeInitializeAnimation()
{

	if (Pawn == nullptr)
	{
		// Get the Owner of the Animation Instance
		Pawn = TryGetPawnOwner(); 

		if (Pawn)
		{
			CompanionCharacter = Cast<ACompanionCharacter>(Pawn);
		}
	}
}

void UCompanionAnimInstance::UpdateAnimationProperties()
{
	if (Pawn)
	{
		FVector Speed = Pawn->GetVelocity();
		FVector LateralSpeed = FVector(Speed.X, Speed.Y, 0);
		MovementSpeed = LateralSpeed.Size(); // Set Velocity without jump vector

		bIsFalling = Pawn->GetMovementComponent()->IsFalling();
	}
}
