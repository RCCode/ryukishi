// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../EnumCollection.h"
#include "MainCharacter.generated.h"




UCLASS()
class RYUKISHI_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	// Holds the Camera on a Stick
	class USpringArmComponent* CameraSpringArm;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivatAccess = "true"))
	// Camera that follows the Character
	class UCameraComponent* CameraToFollow;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Music)
	class UAudioComponent* BackgroundMusic;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Sound)
	class UAudioComponent* SFXGround;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	// Scale turning for Camera
	float BaseTurnRate;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	// Scale look up for Camera
	float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Character)
	// Normal Walkingspeed of Character
	float WalkingSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Character)
	// Walkingsspeed after hitting the MoveFast Key
	float SprintingSpeed;

	UPROPERTY(EditAnywhere, Category = Interaction)
	// Range how far the Character can interact
	float InteractRange;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DeathZ)
	// Z Axis Position on which the Character should respawn
	float DeathZ;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DeathZ)
	FVector ReJumpDirection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = DeathZ)
	float ReJumpForce;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	// Has the key for interactin been pressed
	bool bInteractKeyPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	// Has the key to throw an object been pressed
	bool bThrowKeyPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	// Has the Key to drop an object been pressed
	bool bDropKeyPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	// Has the Key to pause the game been pressed
	bool bPauseKeyPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	bool bSitKeyPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Character)
	bool bIsSitting;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Character)
	// Status of Skeletal Mesh Animation
	EAnimationStatus AnimationStatus;

	UPROPERTY(EditAnywhere, Category = Interaction)
	// Offset for LineTrace from this Actor
	FVector InteractionStartOffset;

	UPROPERTY(EditAnywhere, Category = Interaction)
	// Forceamount of Throw
	float ThrowStrength;

	UPROPERTY(EditAnywhere, Category = Interaction)
	FVector ThrowAngle;

	UPROPERTY(EditAnywhere, Category = Debug)
	// Enable all Debuginformation for Rays on this Object
	bool bEnableDebugRays;

	UPROPERTY(EditAnywhere, Category = Debug)
	bool bDevControls;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Interaction)
	// Current equiped PickUp
	class APickUp* CurrentPickup;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Anim)
	// Animationmontage for Throwing things
	class UAnimMontage* ThrowMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Anim)
	class UAnimMontage* SitMontage;

	UPROPERTY(EditAnywhere, Category = Zoom)
	// Mininmal Zoom of Camera
	float MinZoom;

	UPROPERTY(EditAnywhere, Category = Zoom)
	// Maximal Zoom of Camera
	float MaxZoom;

	UPROPERTY(EditAnywhere, Category = Zoom)
	// Zoom Amount per Input
	float ZoomAmount;

	UPROPERTY(EditAnywhere, Category = Zoom)
	// Interpolationspeed to ZoomTargetValue
	float ZoomInterpSpeed;

	UPROPERTY(EditAnywhere, Category = Sprint)
	// Value to sprint to
	float SprintTargetValue;
	
	UPROPERTY(EditAnywhere, Category = Sprint)
	// Sprint Amount per Input
	float SprintAmount;

	UPROPERTY(EditAnywhere, Category = Sprint)
	// Interpolationspeed to SprintTargetValue
	float SprintInterpSpeed;

	UPROPERTY(EditAnywhere, Category = Music)
	float MusicFadInDuration;

	UPROPERTY(EditAnywhere, Category = Music)
	float MusicFadOutDuration;

	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundCue* InteractingSound;
	
	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundCue* ThrowingSound;

	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundCue* WalkingSound;
	
	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundCue* RunningSound;

	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundCue* JumpSound;

	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundCue* LandSound;

	UPROPERTY(EditAnywhere, Category = Music)
	class USoundCue* MainMusic;

	UPROPERTY(EditAnywhere, Category = Music)
	class USoundCue* NightMusic;

	UPROPERTY(EditAnywhere, Category = Music)
	class USoundCue* ForestMusic;

	UPROPERTY(EditAnywhere, Category = Music)
	class USoundCue* ShrineMusic;

	UPROPERTY(EditAnywhere, Category = Music)
	class USoundCue* DragonMusic;

private:


	UPROPERTY()
	// The current interactable Object the Player is looking at
	class AInteractable* CurrentInteraction;

	UPROPERTY()
	class UInteractableComponent* CurrentComponentInteraction;
	
	UPROPERTY()
	// Value to zoom to
	float ZoomTargetValue;

	UPROPERTY()
	ESoundStatus SoundStatus;

	UPROPERTY()
	EMusicStatus MusicStatus;

	UPROPERTY()
	ETutorialStatus TutorialStatus;

	UPROPERTY()
	int OverlappedMusicCounter;

	UPROPERTY()
	TArray<float> MusicTimeStamps;

	UPROPERTY()
	// Last Position the Character was on Ground
	FVector LastGroundPosition;

	UPROPERTY()
	class AMainPlayerController* MainPlayerController;

private:

	// Create DefaultSubobjects
	void SetupDefaultSubobject();

	// Set Root for Subobjects
	void SetupRoot();

	// Set default Values for Subobjects
	void SetupValues();

	// Bind Action to Key
	void BindInputAction(class UInputComponent* PlayerInputComponent);

	// Bind Axis to Key
	void BindInputAxis(class UInputComponent* PlayerInputComponent);

	// LineTrace so Player can interact with Environmment
	void InteractLineTrace();

	// Start Interaction on Interactable Object
	bool StartInteraction(AInteractable* Interaction);

	void StartInteraction(class UInteractableComponent* Interaction);

	// End Interaction if possible
	void EndInteraction();

	// Set Movementspeed on 0
	void SetMovementSpeed();

	// Set Movementspeed on Value
	void SetMovementSpeed(const float& Speed);

	// Check is PickUp
	class APickUp* CheckIsPickUp(class AInteractable* Interactable);

	// Set CurrentPickUp
	bool SetCurrentPickUp(class AInteractable* Interactable);

	void SwitchMusicStatus(const EMusicStatus& Status);

	// Set the given sound to BackgroundMusic AudioComponent
	void SetBackgroundMusic(class USoundCue*& Sound);

	// Fade to new BackgroundMusic
	void FadeInBackgroundMusic();

	// Set the given sound to SFXGround AudioComponent
	void SetSFXSound(class USoundCue*& Sound);


	// Move this Character to the given Direction
	void MoveCharacter(const float& Value, const EAxis::Type& DirectionAxis);

	// Interpolate to Current Sprinting Speed
	void InterpSprintingSpeed();

	// Check the Z Axis of Character to Respawn
	void CheckDeathZ();

	// Respawn by jumping back into the Playarea
	void ReJump();

	void PlaySitMontage();

	void ChangeTutorial(const ETutorialStatus& SelectedStatus);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Jump() override;

	// Called for forward / backward input
	void MoveForward(float Value);

	// Called for side to side input
	void MoveRight(float Value);

	// Called on Action Input to Move faster
	void MoveFast();

	// Stop Action to Move fast and reset Speed
	void StopMoveFast();

	// Called via input to turn at a given rate
	// Rate = normalized rate, 1.0 means 100% of desired turnrate
	void TurnAtRate(float Rate);

	// Called via input to look up / down at a given rate
	// Rate = normalized rate, 1.0 means 100% of desired turnrate
	void LookUpRate(float Rate);

	// Zoom in or out by Mouse Wheel
	void Zoom(float Rate);

	// Change Sun Rotation with Mouse Wheel for Testpurposes
	void ChangeSunRotation(float Rate);

	// Called if Interaction Key is pressed
	void InteractionPressed();

	// Called on Key is released
	void InteractionReleased();

	// Called on Throw Key is pressed
	void ThrowPressed();

	// Called on Throw is released
	void ThrowReleased();

	// Called if the ESC Key is pressed
	void PausePressed();

	// Called if the ESC Key is released
	void PauseReleased();

	void SitPressed();

	void SitReleased();

	UFUNCTION(BlueprintCallable)
	void ThrowEventObjectReleased();

	// Change the Animation Status
	void SetAnimationStatus(const EAnimationStatus& Status);
	
	// Change the Status of the current CharacterSound
	void SetSoundStatus(const ESoundStatus& Status);

	// Change the Status of the Backgroundmusic
	void SetMusicStatus(const EMusicStatus& Status);


	// Check if AnimationStatus is not this Status
	bool CheckIsNotAnim(const EAnimationStatus& IsNotStatus);

	// Check if AnimationStatus is not multiple Status
	bool CheckIsNotAnim(const TArray<EAnimationStatus>& IsNotStatus);

	UFUNCTION()
	virtual void OnMusicPlayBackPercent(const class USoundWave* PlayingSoundWave, const float PlaybackPercent);

	virtual void OutsideWorldBounds() override;

	FORCEINLINE EAnimationStatus GetAnimationStatus() { return AnimationStatus; }

	FORCEINLINE APickUp* GetCurrentPickUp() { return CurrentPickup; }

	FORCEINLINE EMusicStatus GetMusicStatus() { return MusicStatus; }

};
