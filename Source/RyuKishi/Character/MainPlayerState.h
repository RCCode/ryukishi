// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "MainPlayerState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnCollectScoreChange,const int&, TotalAmount, const int&, CurrentAmount);

UCLASS()
class RYUKISHI_API AMainPlayerState : public APlayerState
{
	GENERATED_BODY()
	
public:
	
	AMainPlayerState();

	UPROPERTY(BlueprintAssignable)
	FOnCollectScoreChange OnCollectScoreChange;

private:

	UPROPERTY()
	int CurrentCollectableAmount;

	UPROPERTY()
	int TotalCollectableAmount;

public:

	void IncrementCollectables();

	void DecrementCollectables(bool bBroadcast = true);
};
