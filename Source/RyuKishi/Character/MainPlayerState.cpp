// Fill out your copyright notice in the Description page of Project Settings.


#include "MainPlayerState.h"

AMainPlayerState::AMainPlayerState()
{
	CurrentCollectableAmount = 0;
	TotalCollectableAmount = 0;
}

void AMainPlayerState::IncrementCollectables()
{
	CurrentCollectableAmount++;
}

void AMainPlayerState::DecrementCollectables(bool bBroadcast)
{
	if (TotalCollectableAmount == 0)
	{
		TotalCollectableAmount = CurrentCollectableAmount;
	}

	CurrentCollectableAmount--;

	if (bBroadcast)
	{
		OnCollectScoreChange.Broadcast(TotalCollectableAmount, CurrentCollectableAmount);
	}

}
