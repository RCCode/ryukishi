// Fill out your copyright notice in the Description page of Project Settings.


#include "MainAnimInstance.h"
#include "GameFramework\CharacterMovementComponent.h"
#include "MainCharacter.h"


void UMainAnimInstance::NativeInitializeAnimation()
{
	if (Pawn == nullptr)
	{
		// Get the Owner of the Animation Instance
		Pawn = TryGetPawnOwner();

		if (Pawn)
		{
			MainCharacter = Cast<AMainCharacter>(Pawn);
		}
	}
}

void UMainAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	if (Pawn)
	{
		if (MainCharacter == nullptr)
		{
			MainCharacter = Cast<AMainCharacter>(Pawn);
		}
		if (bIsInAir && !bHasLanded)
		{
			bHasLanded = true;
		}
		else if (!bIsInAir && bHasLanded)
		{
			MainCharacter->SetSoundStatus(ESoundStatus::Sound_Landing);
			bHasLanded = false;
		}

		FVector Speed = Pawn->GetVelocity();
		FVector LateralSpeed = FVector(Speed.X, Speed.Y, 0);
		MovementSpeed = LateralSpeed.Size();

		bIsInAir = Pawn->GetMovementComponent()->IsFalling();
	}
}
