// Fill out your copyright notice in the Description page of Project Settings.


#include "CompanionCharacter.h"
#include "Components/SphereComponent.h"
#include "AIController.h"
#include "MainCharacter.h"
#include "AITypes.h"
#include "Navigation/PathFollowingComponent.h"
#include "GameFramework\CharacterMovementComponent.h"
#include "Animation/AnimInstance.h"
#include "../GameMechanics/InteractableComponent.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
ACompanionCharacter::ACompanionCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetupDefaultSubobject();
	SetupRoot();
	SetupValues();
}

#pragma region private

void ACompanionCharacter::SetupDefaultSubobject()
{
	ReactionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("ReactionSphere"));
	InteractionParticles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("InteractionParticles"));
}

void ACompanionCharacter::SetupRoot()
{
	ReactionSphere->SetupAttachment(GetRootComponent());
	InteractionParticles->SetupAttachment(GetRootComponent());
}

void ACompanionCharacter::SetupValues()
{
	ReactionSphere->InitSphereRadius(600.f);
	AcceptanceRadius = 25.f;
	FarDistance = 500.f;
	NormalDistance = 250.f;
	JumpZLimit = 1500.f;
	IdleDelay = 2.f;
	bIdleAnimationFinished = true;
	bCanJump = false;
	bWasInAir = false;

	MinDifferenceToPlayerForJump = -75.f;
	MaxDifferenceToPlayerForJump = 300.f;
	ZDifferenceToPlayer = 0.f;

	DeathZ = 436000.f;
	ReJumpDirection = FVector(0.f, 0.f, 2000.f);
	ReJumpForce = 62.f;

	InteractionParticles->SetAutoActivate(false);
}

void ACompanionCharacter::SetWalkSpeed(float& MaxWalkSpeed)
{
	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
}

void ACompanionCharacter::CheckDistance()
{
	float Speed = Player->GetCharacterMovement()->MaxWalkSpeed;

	if (DistanceToPlayer > FarDistance)
	{
		Speed *= 1.5f;
	}
	else if (DistanceToPlayer > NormalDistance)
	{
		// Keep Walking with the same Speed
	}
	else
	{
		Speed *= 0.5f;
		if (CompanionStatus == ECompanionStatus::CS_Idle && GetVelocity().Size() <= 0)
		{
			// Check if Player is moving
			if (DistanceToPlayer > LastDistanceToPlayer)
			{

				if (!bIdleAnimationFinished)
				{
					AnimInstance->Montage_SetPlayRate(IdleMontage, 3.f);
					bIdleAnimationFinished = true;
					StartTimer = 0;
				}
				MoveToTarget(Player);
			}
			else
			{
				// Player is not moving
				if (bIdleAnimationFinished)
				{
					// Delay Idle Animation
					if (Delay(IdleDelay))
					{
						PlayIdleAnimation();
					}
				}
			}
		}
	}

	LastDistanceToPlayer = DistanceToPlayer;
	SetWalkSpeed(Speed);
}

void ACompanionCharacter::InAirBehavior()
{
	// Check for Player is in Air
	if (Player->GetMovementComponent()->IsFalling())
	{
		// Check if we are not in Air
		if (!GetMovementComponent()->IsFalling())
		{

			// Move to valid Point on Navmesh
			AIController->MoveToLocation(FVector(Player->GetActorLocation().X, Player->GetActorLocation().Y, GetActorLocation().Z), AcceptanceRadius);
		}
	}

	if (GetMovementComponent()->IsFalling())
	{
		if (DistanceToPlayer > AcceptanceRadius)
		{
			// Move to Player manually while in Air
			AddMovementInput(Player->GetActorLocation() - GetActorLocation(), 4.f);
		}
		CheckDeathZ();
		bWasInAir = true;
	}
	else 
	{
		if (!CheckForJump())
		{
			if (bWasInAir)
			{
				// Move to Player after landing
				MoveToTarget(Player);
				bWasInAir = false;
			}
			else
			{
				LastGroundPosition = GetActorLocation();
			}
		}
	}
}

bool ACompanionCharacter::CheckForJump()
{
	ZDifferenceToPlayer = (Player->GetActorLocation().Z - GetActorLocation().Z);

	if(ZDifferenceToPlayer > MaxDifferenceToPlayerForJump)
	{
		bCanJump = true;
	}
	else if (ZDifferenceToPlayer < MinDifferenceToPlayerForJump)
	{
		bCanJump = true;
	}
	else
	{
		bCanJump = false;
	}

	if (bCanJump)
	{
		if (ZDifferenceToPlayer < JumpZLimit)
		{
			// Scale Jump with difference
			GetCharacterMovement()->JumpZVelocity = DefaultJumpZVelocity * FMath::Clamp((FMath::Abs(ZDifferenceToPlayer) / MaxDifferenceToPlayerForJump), 0.f, 3.f);
			Jump();
		}
	}

	return bCanJump;
}

void ACompanionCharacter::CheckDeathZ()
{
	if (GetActorLocation().Z < DeathZ)
	{
		ReJump();
	}
}

void ACompanionCharacter::ReJump()
{
	// Reset Velocity & Accelation
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_None);
	GetCharacterMovement()->SetMovementMode(EMovementMode::MOVE_Falling);

	// Add Impulse
	GetCharacterMovement()->AddImpulse(((LastGroundPosition - GetActorLocation()) + ReJumpDirection) * ReJumpForce);
}

void ACompanionCharacter::PlayIdleAnimation()
{
	if (AnimInstance)
	{
		AnimInstance->Montage_Play(IdleMontage);
		AnimInstance->Montage_JumpToSection(IdleMontageNames[FMath::RandRange(0, IdleMontageNames.Num() - 1)], IdleMontage);
		bIdleAnimationFinished = false;
	}
}

bool ACompanionCharacter::Delay(float& DelaySeconds)
{
	if (StartTimer <= 0)
	{
		StartTimer = GetWorld()->GetTimeSeconds();
	}
	else if (GetWorld()->GetTimeSeconds() >= StartTimer + DelaySeconds)
	{
		StartTimer = 0;
		return true;
	}

	return false;
}


#pragma endregion

#pragma region protected

// Called when the game starts or when spawned
void ACompanionCharacter::BeginPlay()
{
	// Needs to be manually added, because Widgets needs to be added to Root of Actor
	InteractableComponent = FindComponentByClass<UInteractableComponent>();

	Super::BeginPlay();

	AIController = Cast<AAIController>(GetController());
	LastGroundPosition = GetActorLocation();
	DefaultJumpZVelocity = GetCharacterMovement()->JumpZVelocity;

	if (IdleMontage && IdleMontageNames.Num() > 0)
	{
		AnimInstance = GetMesh()->GetAnimInstance();
	}

	ReactionSphere->OnComponentBeginOverlap.AddDynamic(this, &ACompanionCharacter::ReactionSphereOnOverlapBegin);
	ReactionSphere->OnComponentEndOverlap.AddDynamic(this, &ACompanionCharacter::ReactionSphereOnOverlapEnd);
	AIController->ReceiveMoveCompleted.AddDynamic(this, &ACompanionCharacter::OnMoveCompleted);

	if (InteractableComponent)
	{
		InteractableComponent->OnInteractableExecute.AddDynamic(this, &ACompanionCharacter::OnInteraction);
	}
}

#pragma endregion

#pragma region public

// Called every frame
void ACompanionCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (Player)
	{
		DistanceToPlayer = (GetActorLocation() - Player->GetActorLocation()).Size2D();
		CheckDistance();

		InAirBehavior();
	}
}

// Called to bind functionality to input
void ACompanionCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ACompanionCharacter::OnInteraction()
{
	InteractionParticles->ResetParticles();
	InteractionParticles->SetActive(true);
	InteractableComponent->bHasInteracted = false;
}

void ACompanionCharacter::ReactionSphereOnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		if (!Player)
		{
			Player = Cast<AMainCharacter>(OtherActor);
		}
		if (Player)
		{
			MoveToTarget(Player);
		}
	}
}

void ACompanionCharacter::ReactionSphereOnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor)
	{
		if (!Player)
		{
			Player = Cast<AMainCharacter>(OtherActor);
		}
		if (Player)
		{
			MoveToTarget(Player);
		}
	}
}

void ACompanionCharacter::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	if (Player)
	{
		SetCompanionStatus(ECompanionStatus::CS_Idle);
	}
}

void ACompanionCharacter::MoveToTarget(AMainCharacter* Target)
{
	if (AIController)
	{

		if (Target)
		{
			SetCompanionStatus(ECompanionStatus::CS_MoveToTarget);
			AIController->MoveToActor((AActor*)Target, AcceptanceRadius);
		}
	}
}

void ACompanionCharacter::IdleAnimationFinished()
{
	bIdleAnimationFinished = true;
}

void ACompanionCharacter::OutsideWorldBounds()
{
	
	AActor* Actor = GetWorld()->SpawnActor<AActor>(GetClass(), LastGroundPosition, FRotator(0));

	ACompanionCharacter* Companion = Cast<ACompanionCharacter>(Actor);

	if (Companion)
	{
		// Spawn AI Controller
		Companion->SpawnDefaultController();

		// Set Controller to Individual Controller, that is normally set in BeginPlay() by C++
		Companion->AIController = Cast<AAIController>(Companion->GetController());
	}

	Super::OutsideWorldBounds();
}

#pragma endregion

