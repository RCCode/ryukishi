// Fill out your copyright notice in the Description page of Project Settings.


#include "MainPlayerController.h"
#include "Blueprint\UserWidget.h"
#include "Kismet\GameplayStatics.h"
#include "MainPlayerState.h"

#define LOCTEXT_NAMESPACE "TextController"

AMainPlayerController::AMainPlayerController()
{
	bPauseMenuVisible = false;
}

void AMainPlayerController::BeginPlay()
{
	MainPlayerState = Cast<AMainPlayerState>(PlayerState);

	if (MainPlayerState)
	{
		MainPlayerState->OnCollectScoreChange.AddDynamic(this, &AMainPlayerController::SetCollectionScoreUI);
	}

	Super::BeginPlay();

	if (WCollectableUI)
	{
		CollectableUI = CreateWidget<UUserWidget>(this, WCollectableUI);
		if (CollectableUI)
		{
			// Sets Order of overall Viewport
			// The last Widget added is always on Top and receives Input
			CollectableUI->AddToViewport();
			CollectableUI->SetVisibility(ESlateVisibility::Visible);
		}

	}


	if (WTutorialText)
	{
		TutorialText = CreateWidget<UUserWidget>(this, WTutorialText);
		if (TutorialText)
		{
			// Sets Order of overall Viewport
			// The last Widget added is always on Top and receives Input
			TutorialText->AddToViewport();
			TutorialText->SetVisibility(ESlateVisibility::Visible);
		}

	}

	if (WPauseMenu)
	{
		PauseMenu = CreateWidget<UUserWidget>(this, WPauseMenu);
		if (PauseMenu)
		{
			// Sets Order of overall Viewport
			// The last Widget added is always on Top and receives Input
			PauseMenu->AddToViewport();
			PauseMenu->SetVisibility(ESlateVisibility::Hidden);
		}
	}

	
}

void AMainPlayerController::DisplayPauseMenu_Implementation()
{
	if (!PauseMenu)
		return;

	PauseMenu->SetVisibility(ESlateVisibility::Visible);
	bPauseMenuVisible = true;

	// Set Input Mode to receive Inputs from UI and Game
	SetInputMode(FInputModeGameAndUI());
	bShowMouseCursor = true;
	UGameplayStatics::SetGamePaused(GetWorld(), true);
}

void AMainPlayerController::HidePauseMenu_Implementation()
{
	if (!PauseMenu)
		return;

	bPauseMenuVisible = false;

	// Set Input Mode to receive Inputs only from Game
	SetInputMode(FInputModeGameOnly());
	bShowMouseCursor = false;
	UGameplayStatics::SetGamePaused(GetWorld(), false);
}

void AMainPlayerController::TogglePauseMenu()
{
	if (bPauseMenuVisible)
	{
		HidePauseMenu();
	}
	else
	{
		DisplayPauseMenu();
	}
}

void AMainPlayerController::SetCollectionScoreUI(const int& TotalAmount, const int& CurrentAmount)
{
	ChangeMessageText(FText::Format(
		LOCTEXT("ChangeMessageText", "{0} von {1} {2}"),
		FText::AsNumber((int32)(TotalAmount - CurrentAmount)),
		FText::AsNumber((int32)TotalAmount),
		FText::FromString(CollectMessage)));
}

void AMainPlayerController::ChangeMessageText_Implementation(const FText& Message)
{
	SetCurrentScoreText(Message);
}

void AMainPlayerController::ChangeMessageText(const FString Message)
{
	ChangeMessageText(FText::Format(LOCTEXT("ChangeMessageText", "{0}"), FText::FromString(Message)));
}

#undef LOCTEXT_NAMESPACE