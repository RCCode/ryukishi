// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class RYUKISHI_API AMainPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	AMainPlayerController();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widget)
	TSubclassOf<UUserWidget> WPauseMenu;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Widget)
	UUserWidget* PauseMenu;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widget)
	TSubclassOf<UUserWidget> WTutorialText;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Widget)
	UUserWidget* TutorialText;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widget)
	TSubclassOf<UUserWidget> WCollectableUI;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Widget)
	UUserWidget* CollectableUI;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Widget)
	bool bPauseMenuVisible;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = PlayerState)
	class AMainPlayerState* MainPlayerState;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category  = Sun)
	class ASunMoon* Sun;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Messages)
	FString CollectMessage;

private:

	FText CurrentScoreText;

protected:

	virtual void BeginPlay() override;

public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "HUD")
	// Implementation for Blueprint Event Function call
	void DisplayPauseMenu();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "HUD")
	// Implementation for Blueprint Event Function call
	void HidePauseMenu();

	void TogglePauseMenu();

	UFUNCTION()
	virtual void SetCollectionScoreUI(const int& TotalAmount, const int& CurrentAmount);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "HUD")
	void ChangeMessageText(const FText& Message);

	void ChangeMessageText(const FString Message);

	UFUNCTION(BlueprintCallable)
	FText GetCurrentScoreText() const { return CurrentScoreText; }

	UFUNCTION(BlueprintCallable)
	void SetCurrentScoreText(FText val) { CurrentScoreText = val; }
};
