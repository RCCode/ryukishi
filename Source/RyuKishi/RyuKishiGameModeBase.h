// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RyuKishiGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class RYUKISHI_API ARyuKishiGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
