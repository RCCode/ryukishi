// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnumCollection.generated.h"

UENUM(BlueprintType)
enum class EAnimationStatus : uint8
{
	EAS_Walking				UMETA(DisplayName = "Walking"),
	EAS_Running				UMETA(DisplayName = "Running"),
	EAS_Interacting			UMETA(DisplayName = "Interacting"),
	EAS_Throwing			UMETA(DisplayName = "Throwing"),
	EAS_Default				UMETA(DisplayName = "Default")
};

UENUM()
enum class EMusicStatus : uint8
{
	Music_Main		UMETA(DisplayName = "Main"),
	Music_Day		UMETA(DisplayName = "Day"),
	Music_Night		UMETA(DisplayName = "Night"),
	Music_Forest	UMETA(DisplayName = "Forest"),
	Music_Shrine	UMETA(DisplayName = "Shrine"),
	Music_Dragon	UMETA(DisplayName = "Dragon"),
	Music_Default	UMETA(DisplayName = "Default")
};

UENUM()
enum class ESoundStatus : uint8
{
	Sound_Walking			UMETA(DisplayName = "SoundWalking"),
	Sound_Running			UMETA(DisplayName = "SoundRunning"),
	Sound_Idle				UMETA(DisplayName = "SoundIdle"),
	Sound_Interacting		UMETA(DisplayName = "SoundInteracting"),
	Sound_CancelInteracting UMETA(DisplayName = "SoundCancelInteracting"),
	Sound_Throwing			UMETA(DisplayName = "SoundThrowing"),
	Sound_Jumping			UMETA(DisplayName = "SoundJumping"),
	Sound_Landing			UMETA(DisplayName = "SoundLanding"),
	Sound_Default			UMETA(DisplayName = "Default")
};

UENUM()
enum class ETutorialStatus : uint8
{
	T_Finished			UMETA(DisplayName = "TutorialFinished"),
	T_Press_E			UMETA(DisplayName = "TutorialPress_E"),
	T_LMB_Throw			UMETA(DisplayName = "TutorialLMB_Throw"),
	T_LMB_Hold			UMETA(DisplayName = "TutorialLMB_Hold"),
	T_E_Drop			UMETA(DisplayName = "TutorialE_Drop"),
	T_Default			UMETA(DisplayName = "Default")
};