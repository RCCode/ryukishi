# Ryu Kíshi
### Guardian of the Dragon Realm

> Open World Third Person Adventure Game

[Click here to watch the Gameplay Trailer](https://www.youtube.com/watch?v=2CgzLJSjbK0)\
<a href="https://www.youtube.com/watch?v=2CgzLJSjbK0"
target="_blank"><img src="http://img.youtube.com/vi/2CgzLJSjbK0/0.jpg" 
width="480" height="380" border="10" /></a>


[Click here to watch another Trailer](https://www.youtube.com/watch?v=TDqeyWZa1cY)\
<a href="ttps://www.youtube.com/watch?v=TDqeyWZa1cY"
target="_blank"><img src="http://img.youtube.com/vi/TDqeyWZa1cY/0.jpg" 
width="480" height="380" border="10" /></a>




Project made in collaboration with [@OokamiRei](https://www.artstation.com/reigameart) (Game Art)

Project Engine: Unreal Engine 4.26
